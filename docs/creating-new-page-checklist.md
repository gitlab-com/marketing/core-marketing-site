# Creating a new page template

Since adding Contentful as our content repository into our tech stack, it has made creating a new Vue page template slightly more complex than it used to be.

Thank you for following our code standards listed here and helping us keep our repository organized, stable, and as bug-free as possible!

## Add necessary base imports

```
import Vue from 'vue';
import { MetaInfo } from 'vue-meta/types/vue-meta';
import { getPageMetadata } from '~/common/meta';
import { CONTENT_TYPES } from '~/common/content-types';
import { getClient } from '~/plugins/contentful';
import { LivePreviewMixin } from '~/mixins';
import { ContentfulLivePreview } from '@contentful/live-preview';
import { convertToCaseSensitiveLocale } from '~/common/util'; // Required only if the page has multiple locales
```

## Add necessary mixins

All new pages should have the Live Preview in Contentful enabled as well as "Edit page" footer link information.

```
import { LivePreviewMixin, FooterEditLink } from '~/mixins';

....

mixins: [LivePreviewMixin, FooterEditLinkMixin],

```

Please see our docs regarding `embedded-videos` and `images` to learn more about those important mixins or utilities.

## Service files

The business logic required to massage data from Contentful's format into a flexible, readible format on our side is extensive. This logic should be handled in a separate file from the page, imported, and used as a single function. These files live in our `/services` folder.

Service files should allow for page flexibility and not grab specific objects or values at certain index in an array. Consider using a combination of the Content Type ID, a switch statement, and if applicable, the value of the `Component Name` field. That logic, in addition to a certain way to render the template, will allow the page to render in a way that the Contentful editor wants and remain mostly error-free.

```
// Template example:

<template>
<component
    :is="component.name"
    v-for="component in data.components"
    :key="component.name"
    :data="component.data"
/>
</template>
```

## AsyncData function

1. Ensure there is a `try/catch` with proper error handling
2. Use the `getEntries` Contentful SDK functino over `getEntry`. getEntries sends back a response that includes the JSON data to linked, or child, entries. 
3. We typically add a param to grab an entry by `slug` or by entry ID. It is better practice to use the slug value, but some cases require an ID.
4. If you also need localized content, be sure to configure the `locale` param
5. Use the `include` param on the call as well. Consider a value that allows you to grab child references
6. Use `select` or `limit` params if you have a use case where they could be helpful
7. Use your service file's main function to format data properly

