---
  title: "Migration von Atlassian zu GitLab"
  description: Atlassian beendet im Februar 2024 den Support für alle Server-Produkte, darunter auch für Bitbucket, Jira, Bamboo und Confluence. Lass dich von dieser Änderung nicht dazu zwingen, ein Tool zu verwenden, das nicht für dein Team geeignet ist. Erfahre, wie GitLab dich unterstützen kann.
  side_navigation_links:
    - title: Bitbucket
      href: '#bitbucket'
      data_ga_name: bitbucket
      data_ga_location: navigation
    - title: Jira
      href: '#jira'
      data_ga_name: jira
      data_ga_location: navigation
    - title: Bamboo
      href: '#bamboo'
      data_ga_name: bamboo
      data_ga_location: navigation
    - title: Confluence
      href: '#confluence'
      data_ga_name: confluence
      data_ga_location: navigation
    - title: Preise
      href: '#pricing'
      data_ga_name: pricing
      data_ga_location: navigation
  side_navigation_text_link:
    text: Vertrieb kontaktieren
    url: '/sales/'
    data_ga_name: sales
    data_ga_location: navigation
  solutions_hero:
    title: Migration von Atlassian zu GitLab
    badge:
      text: GitLab Enterprise Agile Planning jetzt als Add-on verfügbar
      url: "#agile-add-on"
      icon: gl-arrow-right
      data_ga_name: agile delivery add-on
      data_ga_location: hero
    isDark: true
    subtitle: Atlassian beendet im **Februar 2024** den Support für alle Server-Produkte, darunter [Bitbucket](#bitbucket), [Jira](#jira), [Bamboo](#bamboo) und [Confluence](#confluence). Lass dich nicht dazu zwingen, ein Tool zu verwenden, das nicht für dein Team geeignet ist. Erfahre, wie GitLab dich unterstützen kann.
    primary_btn:
      text: Sprich mit einem Experten/einer Expertin
      url: /sales/
      data_ga_name: sales
      data_ga_location: header
    image:
      image_url: "https://images.ctfassets.net/xz1dnu24egyd/26SlAGBx32ZPYt2I4Wcd9A/95e7a4353d19da0b6a3eaff250e8205c/atlassian-eol-landing.svg"
      alt: "devops lifecycle image"
      bordered: true
  content_card:
    title: 'Wechsle von Bitbucket zu GitLab'
    description: |
      **Zusammenarbeit und Beschleunigung**


      Stelle immer bereit mit Versionskontrolle für Ressourcen, Feedbackschleifen und leistungsstarken Branching-Mustern, damit deine Entwickler(innen) Probleme effizient lösen und eine wahren Mehrwert liefern können.


      

      **Compliance und Sicherheit**


      Ermögliche es den Teams, Codeänderungen mit einer Single Source of Truth zu überprüfen, nachzuverfolgen und freizugeben.
    link: /
    type: "eBook"
    image: https://images.ctfassets.net/xz1dnu24egyd/GF2chSP6KXImk3djWqJxk/d0119e7396e05f112e77583415405c10/principle-1.png
    icon: open-book
    button_text: 'E-Book herunterladen'
    size: half
    button_type: primary
    image_right: true

  benefit_cards:
    title: GitLab ist der schnellste Weg von der Idee zur Software
    benefits:
      - title: Stelle überall bereit
        icon: gitlab-cloud
        description: Wähle aus, wie und wo du bereitstellen möchtest. GitLab ist auch für Kund(inn)en da, die eine selbst gehostete Lösung brauchen.
      - title: Optimiere die Softwareentwicklung
        icon: collaboration
        description: Unzusammenhängende Toolchains gehören der Vergangenheit an. Stelle deinen Teams eine einzige Plattform für den Software-Entwicklungsprozess bereit, um Bearbeitungszeiten zu reduzieren, die Produktivität zu steigern und die Entwicklungskosten zu senken.
      - title: Lege dein eigenes Tempo fest
        icon: monitor-gitlab
        description: Du bist noch nicht bereit, dich auf einmal von allen Tools und Diensten zu verabschieden? Kein Problem. GitLab bietet auch eine umfassende Auswahl an Integrationen, damit du deine Migration in deinem eigenen Tempo vornehmen kannst.

  copy_benefits_bitbucket:
    title: Wechsle von Bitbucket zu GitLab
    subtitle: Versionskontrolle für alle
    background: true
    benefits:
      - title: Leistungsstarke Versionskontrolle
        icon: idea-collaboration
        description: Stelle immer bereit mit Versionskontrolle für Ressourcen, Feedbackschleifen und leistungsstarken Branching-Mustern, damit deine Entwickler(innen) Software effizient testen und liefern können.
      - title: Integrierte Sicherheit und Compliance
        icon: devsecops
        description: Ermögliche deinen Teams, Codeänderungen mit einer Single Source of Truth zu überprüfen, nachzuverfolgen und freizugeben.
        button_text: Mehr erfahren
        button_url: /solutions/source-code-management/
        data_ga_name: bitbucket
        data_ga_location: body

  copy_benefits_jira:
    title: Wechsle von Jira zu GitLab
    subtitle: Integrierte Agile-Unterstützung für Projekte, Programme und Produkte
    benefits:
      - title: Agile-Planung
        icon: agile
        description: Generiere schneller Geschäftswerte, indem du standardmäßige Agile-Funktionen für Team- und Portfoliomanagement bereitstellst – und dies noch dazu in der gleichen Softwarebereitstellungsplattform, die auch verwendet wird, um Software zu erstellen, zu testen und bereitzustellen.
        button_text: Mehr erfahren
        button_url: /solutions/agile-delivery/
        data_ga_name: agile delivery
        data_ga_location: jira body
      - title: Wertstrommanagement
        icon: visibility
        description: Visualisiere Wertströme und eliminiere Engpässe im gesamten Software-Entwicklungsprozess, ordne Prioritäten schnell neu, um die Produktivität zu verbessern, und schaffe Geschäftswerte.
        button_text: Mehr erfahren
        button_url: /solutions/value-stream-management/
        data_ga_name: value stream management
        data_ga_location: jira body

  copy_benefits_confluence:
    title: Wechsle von Confluence zu GitLab
    subtitle: Wissensmanagement zum Abbau von Kommunikationssilos
    benefits:
      - title: Anpassbare Wiki-Seiten
        icon: web-alt
        description: Bewahre organisatorisches Wissen und Dokumentation direkt in GitLab auf, um einen einfachen Zugriff für alle Teams und Positionen zu ermöglichen.
        button_text: Mehr erfahren
        button_url: https://docs.gitlab.com/ee/user/project/wiki/
        data_ga_name: confluence
        data_ga_location: body

  copy_benefits_bamboo:
    title: Wechsle von Bamboo zu GitLab
    background: true
    subtitle: Flexible CI/CD, um die Softwarebereitstellung skalierbar und bedarfsgerecht zu gestalten
    benefits:
      - title: Pipelines, die auf Einfachheit und Skalierbarkeit ausgelegt sind
        icon: monitor-pipeline
        description: Beginne ganz einfach mit integrierten Vorlagen und skaliere dann mit über- und untergeordneten Pipelines und Merge-Zügen.
      - title: Integrierte Funktionen für Sicherheit und Compliance
        icon: shield-check-large-light
        description: Von Compliance-Pipelines bis hin zu integriertem Sicherheitsscans – du findest alles an einem Ort für mehr Transparenz und Kontrolle.
        button_text: Mehr erfahren
        button_url: /solutions/security-compliance/
        data_ga_name: security compliance
        data_ga_location: bamboo body
      - title: Kontextabhängige Tests
        icon: monitor-test-2
        description: Teste alles automatisch – von der Code-Performance bis zur Sicherheit – und überprüfe und genehmige die Ergebnisse im Kontext.
        button_text: Mehr erfahren
        button_url: /solutions/continuous-integration/
        data_ga_name: bamboo
        data_ga_location: body

  resources:
    col_size: 4
    header: "Ressourcen für die Migration von Atlassian zu GitLab"
    description: Erfahre mehr darüber, warum Atlassian den Server-Support beendet und wie du zu GitLab wechselst.
    case_studies:
      - header: "Beyond Devs: GitLab Enterprise Agile Planning add-on for all roles (nur in englischer Sprache verfügbar)"
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/
          text: Mehr erfahren
          data_ga_name: enterprise agile planning resource
          data_ga_location: body
      - header: Why GitLab self-managed is the perfect partner for the public sector (nur in englischer Sprache verfügbar)
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/3wDRL8ssQu72bNfsvN1Sds/1b48bc3c8cb63eca3f47cf40b99a6308/gitlabflatlogomap.jpg
          alt: ""
        link:
          href: /blog/2023/12/13/why-gitlab-self-managed-is-the-perfect-partner-for-the-public-sector/
          text: Mehr erfahren
          data_ga_name: perfect partner resource
          data_ga_location: body
      - header: "Atlassian Server ending: Goodbye disjointed toolchain, hello DevSecOps platform (nur in englischer Sprache verfügbar)"
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/HE9XYEbagTAiFYXohe8p9/47f0ab25fda5c00db236e7bb91790492/value-stream.png
          alt: ""
        link:
          href: /blog/2023/09/26/atlassian-server-ending-move-to-a-single-devsecops-platform/
          text: Mehr erfahren
          data_ga_name: atlassian resource
          data_ga_location: body
      - header: 5 reasons to simplify your agile planning tool configuration with GitLab (nur in englischer Sprache verfügbar)
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/10/17/five-reasons-to-simplify-agile-planning-tool-configuration-gitlab/
          text: Mehr erfahren
          data_ga_name: simplify agile resource
          data_ga_location: body
      - header: Tips for a successful Jira to GitLab migration (nur in englischer Sprache verfügbar)
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/10/24/tips-for-a-successful-jira-to-gitlab-migration/
          text: Mehr erfahren
          data_ga_name: Tips for a successful Jira to GitLab migration resource
          data_ga_location: body
      - header: How to migrate from Bamboo to GitLab CI/CD (nur in englischer Sprache verfügbar)
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/26eTPBKrWglfy1ILxWSUsD/7230b6d753d78b80abbcfe736449e218/securitylifecycle-light.png
          alt: ""
        link:
          href: /blog/2023/10/26/migrating-from-bamboo-to-gitlab-cicd/
          text: Mehr erfahren
          data_ga_name: How to migrate from Bamboo to GitLab CI/CD resource
          data_ga_location: body
      - header: Import your project from Bitbucket Cloud to GitLab (nur in englischer Sprache verfügbar)
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/7veFR11eLgKvggQfNISDgK/d0ecb9098c5b913e22d90790d8cb7391/bitbucket.png
          alt: ""
        link:
          href: https://docs.gitlab.com/ee/user/project/import/bitbucket.html
          text: Mehr erfahren
          data_ga_name: bitbucket resource
          data_ga_location: body
      - header: Import your Jira project issues to GitLab (nur in englischer Sprache verfügbar)
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/5mnlgW5gqy5JBvE819dH2q/25521343423e026ada32fbd7daf80b5a/jira.png
          alt: ""
        link:
          href: https://docs.gitlab.com/ee/user/project/import/jira.html
          text: Mehr erfahren
          data_ga_name: jira resource
          data_ga_location: body
      - header: 1 billion pipelines of CI/CD innovation (nur in englischer Sprache verfügbar)
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/19z3IxRhCpQf37oD2lEOHN/34faaf9fb0fd927efe6c30a10f74c499/securitylifecycle.png
          alt: ""
        link:
          href: /blog/2023/10/04/one-billion-pipelines-cicd/
          text: Mehr erfahren
          data_ga_name: pipeline resource
          data_ga_location: body
  tier_block:
    header: Welcher Tarif ist der richtige für dich?
    cta:
      url: /pricing/
      text: Welcher Tarif ist der richtige für dich?
      data_ga_name: pricing
      data_ga_location: free tier
      aria_label: Preise
    tiers:
      - id: free
        title: Free
        items:
          - Statische Anwendungssicherheitstests (SAST) und Geheimniserkennung
          - Ergebnisse in JSON-Datei
        link:
          href: /pricing/
          text: Mehr erfahren
          data_ga_name: pricing
          data_ga_location: free tier
          aria_label: Kostenloser Tarif
      - id: premium
        title: Premium
        items:
          - Statische Anwendungssicherheitstests (SAST) und Geheimniserkennung
          - Ergebnisse in JSON-Datei
          - MR-Genehmigungen und weitere gängige Kontrollen
        link:
          href: /pricing/
          text: Mehr erfahren
          data_ga_name: pricing
          data_ga_location: premium tier
          aria_label: Premium-Tarif
      - id: ultimate
        title: Ultimate
        items:
          - Alle Funktionen von Premium und zusätzlich
          - Umfassende Sicherheitsscanner wie SAST, DAST, Geheimnisse, Abhängigkeiten, Container, IaC, APIs, Cluster-Images und Fuzz-Tests
          - Umsetzbare Ergebnisse innerhalb der MR-Pipeline
          - Konformitäts-Pipelines
          - Sicherheits- und Konformitäts-Dashboards
          - Und viel mehr
        link:
          href: /pricing/
          text: Mehr erfahren
          data_ga_name: pricing
          data_ga_location: ultimate tier
          aria_label: Ultimate-Tarif
        cta:
          href: /free-trial/
          text: Starte deine kostenlose Testversion
          data_ga_name: pricing
          data_ga_location: ultimate tier
          aria_label: Ultimate-Tarif

  pricing_banner:
    id: agile-add-on
    badge_text: Neu
    headline: GitLab Enterprise Agile Planning
    blurb: Zusätzliche Agile-Planning-Plätze für GitLab-Ultimate-Kund(inn)en.
    button:
      text: Preise auf Anfrage
      url: "/sales/"
      data_ga_name: contact us for pricing
      data_ga_location: body
    footnote: ''
    features:
      header: 'Agile-Planning-Lösung für Unternehmen:'
      list:
        - text: Ersatz für Jira
        - text: Ein Planungsworkflow für alle, die am Softwareentwicklungszyklus beteiligt sind
        - text: Wertstromanalyse zur Messung von Geschwindigkeit und Auswirkung
        - text: Executive-Dashboards für unternehmensweite Transparenz
        - text: Eigenständige Enterprise-Agile-Planning-Plätze für GitLab-Ultimate-Kund(inn)en


  recognition:
    heading: GitLab ist die führende DevSecOps-Plattform
    stats:
      - value: Mehr als 50 %
        blurb: Fortune 100
      - value: Über 30 Mio.
        blurb: registrierte Benutzer(innen)
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: gartner logo
        text: 'GitLab ist ein Leader im Gartner® Magic Quadrant™ für DevOps-Plattformen 2024'
        link:
          text: Bericht lesen
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: forrester logo
        text: "GitLab ist der einzige Marktführer in The Forrester Wave™: Integrierte Softwarebereitstellungsplattformen, Q2\_2023"
        link:
          text: Bericht lesen
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
      - header: 'GitLab gilt in allen DevSecOps-Kategorien als G2-Leader'
        link:
          text: Was Branchenanalyst(inn)en über GitLab sagen
          href: /analysts/
          data_ga_name: g2
          data_ga_location: analyst
        badges:
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 Enterprise Leader  – Sommer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MomentumLeader_Leader.png
            alt: G2 Momentum Leader – Sommer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MostImplementable_Total.svg
            alt: G2 Most Implementable – Sommer 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestResults_Total.png
            alt: G2 Best Results – Sommer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Enterprise_Total.png
            alt: G2 Best Relationship Enterprise – Sommer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Mid-Market_Total.svg
            alt: G2 Best Relationship Mid-Market – Sommer 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 Easiest to use – Sommer 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestUsability_Total.png
            alt: G2 Best Usability – Sommer 2023

  atlassian_solutions:
    title: GitLab lässt sich in Hunderte von bestehenden Anwendungen integrieren
    subtitle: Möchtest du wissen, wie deine Produkt- und Entwicklungsteams besser zusammenarbeiten können, ohne die Tools zu wechseln? [Kontaktiere uns, um mehr über kundenspezifische Lösungen zu erfahren](/sales/).
    solutions:
      - title: Jira
        subtitle: Einfache [Atlassian-Jira-Integration](https://docs.gitlab.com/ee/integration/jira/){data-ga-name="jira docs" data-ga-location="body"}
      - title: GitHub
        subtitle: Nahtlose [GitLab CI/CD mit GitHub  SCM](https://docs.gitlab.com/ee/user/project/integrations/github.html){data-ga-name="github docs" data-ga-location="body"}
      - title: Jenkins
        subtitle: Gepflegtes [GitLab-Plugin](https://docs.gitlab.com/ee/integration/jenkins.html){data-ga-name="jenkins docs" data-ga-location="body"}
      - title: APIs
        subtitle: '[APIs](https://docs.gitlab.com/ee/api/integrations.html){data-ga-name="apis" data-ga-location="body"} für jede GitLab-Komponente'
