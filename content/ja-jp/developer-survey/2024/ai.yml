title: DevSecOpsにおけるAI導入のトレンドと進化
og_title: DevSecOpsにおけるAI導入のトレンドと進化
description: >-
  AIの導入は急ピッチで広まっていますが、当社の調査によると、ほとんどの組織は、いまだにソフトウェア開発ライフサイクルにAIをどのように組み込むかの検討段階にあります。導入済みの組織も、まだ検討中の組織も、今後AIを活用していく中で何が待ち受けているのか、その可能性についてご紹介します。
twitter_description: >-
  AIの導入は急ピッチで広まっていますが、当社の調査によると、ほとんどの組織は、いまだにソフトウェア開発ライフサイクルにAIをどのように組み込むかの検討段階にあります。導入済みの組織も、まだ検討中の組織も、今後AIを活用していく中で何が待ち受けているのか、その可能性についてご紹介します。
og_description: >-
  AIの導入は急ピッチで広まっていますが、当社の調査によると、ほとんどの組織は、いまだにソフトウェア開発ライフサイクルにAIをどのように組み込むかの検討段階にあります。導入済みの組織も、まだ検討中の組織も、今後AIを活用していく中で何が待ち受けているのか、その可能性についてご紹介します。
og_image: /nuxt-images/developer-survey/2024/2024-devops-survey-ai-meta.jpg
twitter_image: /nuxt-images/developer-survey/2024/2024-devops-survey-ai-meta.jpg
form:
    tag: 無料でアクセス
    header: レポート全文を読むにはご登録ください
    text: |
      世界39か国5,000人以上のDevSecOpsのエキスパートを対象に実施した、興味深い情報が満載のレポート全文を無料でご覧いただけます。

      **レポートの内容**

      - DevSecOpsでのAI活用における4つのステージ
      - AIブームが落ち着いた今こそ、じっくり考えて導入を
      - AIの導入はすでに避けては通れないものに
      - 組織はソフトウェア開発ライフサイクル全体でAIを導入
      - セキュリティとスキルがAIの導入を遅らせる原因に
      - AIの影響を理解する：次のフロンティア

    formId: 1002 # Localized versions use a separate form ID
    formDataLayer: resources
    submitted:
      tag: 送信が完了しました
      text: AIについての情報が満載の2024年グローバルDevSecOpsレポートがまもなくメールで送信されます。
      img_src: '/nuxt-images/developer-survey/2024/ai-report-img.png'
hero:
  year: 2024年グローバルDevSecOpsレポート
  title: DevSecOpsにおけるAI導入のトレンドと進化
  img_src: '/nuxt-images/developer-survey/2024/ai-report-img.png'
  mobile_img_src: '/nuxt-images/developer-survey/2024/ai-mobile-hero-img.svg'
intro:
  survey_header:
    text: >-
      AIの導入は急ピッチで広まっていますが、当社の調査によると、ほとんどの組織は、いまだにソフトウェア開発ライフサイクルにAIをどのように組み込むかの検討段階にあります。導入済みの組織も、まだ検討中の組織も、今後AIを活用していく中で何が待ち受けているのか、その可能性についてご紹介します。
    img_variant: 1
  summary:
    title: 概要
    text: |
      本レポートは、Omdia社とGitLabが実施した調査結果を分析したものです。この調査では、世界中の5,000人以上のソフトウェア開発担当者、セキュリティ担当者、オペレーション担当者に対して、DevSecOpsの方針とアプローチに関する企業の立場と導入状況について尋ねました。

      本年度の調査では、人工知能（AI）がソフトウェア開発の主流になりつつある一方で、ソフトウェア開発ライフサイクルにおけるAIの導入に関しては、組織がさまざまなステージにいることが明らかになりました。ほとんどの企業では、AIのブームを超えて、特定のユースケースにどのように活用できるかを模索し始めています。また、いち早くAIを導入した企業では、アプローチを最適化し、AIがソフトウェア開発プロセスに与える影響を実証し始めています。

  highlights:
    title: ハイライト
    type: barGraph
    buttons:
      previous: 前へ
      next: 次へ
    gaLocation: summary
    blocks:
      - text: >-
          <div class="stat orange"><span class="number">60</span>%</div> の回答者が、取り残されないためにはソフトウェア開発プロセスにAIを導入することが不可欠であると回答
        dataGaName: ai is essential
      - text: >-
          <div class="stat teal"><span class="number">59</span>%</div> の回答者が、自社の組織はAIを導入する準備ができていると回答
        dataGaName: ai preparedness
      - text: >-
          <div class="stat red"><span class="number">39</span>%</div> の回答者が、現在ソフトウェア開発にAIを使用していると回答し、2023年から16%増加
        dataGaName: ai usage
      - text: >-
          <div class="stat purple"><span class="number">55</span>%</div> の回答者が、ソフトウェア開発ライフサイクルにAIを導入することはリスクが高いと回答
        dataGaName: ai risk