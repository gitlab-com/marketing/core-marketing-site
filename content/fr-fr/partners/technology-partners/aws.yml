seo: 
  title: GitLab et AWS
  description: 'GitLab et AWS s''associent pour offrir la plateforme DevSecOps alimentée par l''IA la plus complète et dotée des fonctionnalités de cloud computing les plus avancées.'
content: 
  hero:
    title: GitLab et AWS
    pill: 
      text: 'Nouveau : GitLab Duo combiné à Amazon Q'
      button:
        href: '#interest'
        text: En savoir plus
        data_ga_name: 'learn more'
        data_ga_location: 'hero'
      gradient: 'linear-gradient(135deg, #F7A20F,#A989F5,#962EFF,#0099FF)'
    subtitle: GitLab et AWS s'associent pour offrir la plateforme DevSecOps alimentée par l'IA la plus complète et dotée des fonctionnalités de cloud computing les plus avancées.
    image: 
      image_url: https://images.ctfassets.net/xz1dnu24egyd/72URY4Vg59wKsYMpTqrMel/194347274767abce3c04be7be72a75a3/AWS-Logo__1_.png
    primary_btn:
      text: Acheter GitLab Ultimate
      url: https://aws.amazon.com/marketplace/pp/prodview-si5mlpxc22ni2?sr=0-2&ref_=beagle&applicationId=AWSMPContessa
      data_ga_name: buy ultimate
      data_ga_location: hero
      icon:
        name: external-link
        variant: product
    secondary_btn:
      text: Acheter GitLab Premium
      url: https://aws.amazon.com/marketplace/pp/prodview-vehcu2drxakic?sr=0-1&ref_=beagle&applicationId=AWSMPContessa
      data_ga_name: buy premium
      data_ga_location: hero
      variant: primary
      icon:
        name: external-link
        variant: product
    footnote: Pas encore décidé à acheter ? [Commencez un essai gratuit](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/partners/technology-partners/aws//&glm_content=default-saas-trial)
  section:
    title: Accélérez la livraison d'applications modernes avec la sécurité, l'IA et la mise à l'échelle intégrées
    description: Avec GitLab et AWS, vous bénéficiez de capacités d'IA avancées et d'une automatisation complète à chaque étape du développement logiciel, le tout sur l'infrastructure la plus sécurisée, la plus puissante et la plus fiable qui soit. Offrez à votre équipe des fonctionnalités intégrées de sécurité, de mise à l'échelle et de fiabilité qui lui permettent de gérer au sein d'une seule plateforme tous les aspects du déploiement peu importe le type de charge de travail.
    cards:
      - title: Gagnez en efficacité opérationnelle
        description: Restez en tête de la concurrence en combinant la plateforme DevSecOps complète de GitLab aux puissantes fonctionnalités cloud d'AWS dans une solution unique et intégrée.
        config: 
          icon: cogs
          variant: marketing
      - title: Accélérez l'adoption du cloud
        description: Adaptez-vous rapidement à mesure que les besoins évoluent grâce à des options de déploiement flexibles sur une infrastructure AWS fiable qui prend en charge votre stratégie cloud à l'échelle de l'entreprise.
        config: 
          icon: gitlab-cloud
          variant: marketing
      - title: Augmentez la productivité des développeurs
        description: Tirez parti de la plateforme DevSecOps alimentée par l'IA la plus complète, optimisée par les agents autonomes Amazon Q pour accompagner les développeurs à chaque étape de leur workflow.
        config: 
          icon: speed-gauge
          variant: marketing
    quote: 
      logo: https://images.ctfassets.net/xz1dnu24egyd/71Lato37uFeMg1qzLmxLaW/28db7308c383cd2b80ad1cfee438f392/axway-logo.svg
      text: « Les fonctionnalités de GitLab sur site étaient plus sophistiquées que celles de GitHub. Nous avons constaté que le rythme et le cycle de développement [de GitLab] étaient plus rapides, et que la communauté contribuait activement à la livraison des produits. »
      name: Eric Labourdette
      title: Head of Global R&D Engineering Services, Axway
  videoLooping:
    videoUrl: 'https://player.vimeo.com/video/1033653810?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    mp4Url: '/nuxt-images/partners/aws/gitlab-aws.mp4'
    webmUrl: '/nuxt-images/partners/aws/gitlab-aws.webm'
  formSection:
    image: imageTest
    description: Accélérez le développement d'applications modernes en combinant une plateforme DevSecOps alimentée par l'IA à des agents d'IA autonomes dans un workflow unique.
    button:
      text: Consulter l'annonce
      config:
        href: 'https://about.gitlab.com/blog/2024/12/03/gitlab-duo-with-amazon-q-devsecops-meets-agentic-ai/'
        dataGaName: read the announcement
        dataGaLocation: 'body'
    items: 
      - header: Des agents d'IA intégrés à votre workflow de développement
        text: 'GitLab Duo combiné à Amazon Q introduit des fonctionnalités innovantes qui automatisent une variétés de tâches, allant de la planification et du développement de fonctionnalités à la génération de tests unitaires pilotés par l''IA, en passant par les révisions automatisées des merge requests et les améliorations apportées au code base Java.'
        icon:
          name: ai-code-suggestions
          variant: marketing
      - header: La sécurité et la conformité à l'échelle de l'entreprise
        text: 'Sécurisez vos logiciels à l''aide d''outils intégrés de gestion des vulnérabilités et de politiques de conformité qui permettent de détecter, de hiérarchiser et de résoudre les problèmes de sécurité, directement au sein des workflows de l''équipe de développement.'
        icon:
          name: ai-vulnerability-resolution
          variant: marketing
      - header: Une expérience développeur fluide
        text: "Livrez de meilleurs produits plus rapidement avec la plateforme DevSecOps complète alimentée par l'IA de GitLab et les agents d'IA autonomes d'Amazon\_Q intégrés dans un workflow unique et collaboratif."
        icon: 
          name: values
          variant: marketing
  form: 
    formId: 28439
    form_subheader: Liste d'attente
    form_header: Découvrez GitLab Duo combiné à Amazon Q
    formDataLayer: partner
    submitted_message:
      header: "Vous avez été ajouté à la liste d'attente."
  accordion:
    is_accordion: true
    title: Découvrez les solutions communes GitLab et AWS
    description: >-
      En tant que partenaire technologique certifié AWS Advanced avec une compétence DevOps, GitLab CI/CD est un modèle éprouvé pour garantir le succès client sur la plateforme cloud leader du marché. Les clients AWS ont le choix entre deux options de déploiement : **GitLab Auto-géré** et l'**édition SaaS de GitLab.**


      Installez, administrez et maintenez votre propre instance GitLab sur tout type de support (serveur physique dédié, VM et conteneurs) sur AWS avec GitLab Auto-géré. L'édition SaaS de GitLab ne nécessite aucune installation, vous pouvez donc vous inscrire et vous lancer rapidement. 
    items: 
    - header: Amazon Elastic Compute Cloud (EC2)
      text: 'Amazon EC2 fournit une capacité de cloud computing AWS évolutive. GitLab met les jobs à l''échelle sur plusieurs machines. Et GitLab combiné à EC2 permet de réduire de manière significative les coûts d''infrastructure.'
      link_text: En savoir plus
      link_url: "https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/"
      data_ga_name: amazon elastic compute cloud
      data_ga_location: body
    - header: AWS Fargate
      text: 'AWS Fargate permet, en un clic sur GitLab, de réaliser des déploiements serverless et évolutifs de conteneurs. Les entreprises migrent vers Fargate pour optimiser leurs ressources informatiques et réduire leurs coûts d''infrastructure. Fargate utilise une pile AWS comprenant ECS ou Amazon EKS.'
      link_text: En savoir plus
      link_url: "https://docs.gitlab.com/runner/configuration/runner_autoscale_aws_fargate/"
      data_ga_name: aws fargate
      data_ga_location: body
    - header: Amazon Elastic Kubernetes Services (EKS)
      text: 'AWS Elastic Kubernetes Service (EKS) est un service Kubernetes géré. GitLab CI/CD offre une création de cluster intégrée pour Amazon EKS. Amazon EKS est le seul service Kubernetes qui permet aux utilisateurs actuels d''AWS de profiter d''une intégration étroite avec d''autres services et fonctionnalités AWS. GitLab prend également en charge Amazon EKS-D.'
      link_text: En savoir plus
      link_url: "https://docs.gitlab.com/ee/ci/cloud_deployment/ecs/deploy_to_aws_ecs.html"
      data_ga_name: aws fargate
      data_ga_location: body
    - header: AWS Lambda
      text: 'AWS Lambda est un service de calcul qui exécute du code en réponse à des événements et gère automatiquement les ressources de calcul requises par ce code. GitLab prend en charge le développement des fonctions Lambda et d''applications serverless avec AWS Serverless Application Model (AWS SAM) et GitLab CI/CD.'
      link_text: En savoir plus
      link_url: "/blog/2020/04/29/aws-gitlab-serverless-webcast/"
      data_ga_name: aws lambda
      data_ga_location: body
    - header: AWS Elastic Container Service (Amazon ECS)
      text: 'AWS Elastic Container Service (Amazon ECS) est un service de gestion de conteneurs. Gagnez du temps lorsque vous exécutez des commandes AWS à partir de GitLab CI/CD et automatisez les déploiements Docker avec les modèles d''intégration continue (CI) de GitLab.'
      link_text: En savoir plus
      link_url: "https://docs.gitlab.com/ee/ci/cloud_deployment/#deploy-your-application-to-the-aws-elastic-container-service-ecs"
      data_ga_name: aws elastic container service
      data_ga_location: body
    - header: Windows .Net sur AWS
      text: 'GitLab active les pipelines CI/CD pour les applications Windows .Net sur AWS. Déployez automatiquement des applications conteneurisées, y compris des ressources serverless, avec GitLab sur Lambda ou Fargate.'
      link_text: En savoir plus
      link_url: "https://www.youtube.com/watch?v=_4r79ZLmDuo&feature=youtu.be"
      data_ga_name: windows net on aws
      data_ga_location: body