---
  title: Perché GitLab Premium?
  description: Migliora la produttività e il coordinamento del team con GitLab Premium.
  side_menu:
    anchors:
      text: In questa pagina
      data:
      - text: Panoramica
        href: "#overview"
        data_ga_name: overview
        data_ga_location: side-navigation
        nodes:
        - text: Riepilogo
          href: "#wp-summary"
          data_ga_name: summary
          data_ga_location: side-navigation
        - text: Soluzioni chiave
          href: "#wp-key-solutions"
          data_ga_name: key-solutions
          data_ga_location: side-navigation
        - text: Case study dei clienti
          href: "#wp-customer-case-studies"
          data_ga_name: customer-case-studies
          data_ga_location: side-navigation
      - text: Calcolatore del ROI
        href: "#wp-roi-calculator"
        data_ga_name: roi-calculator
        data_ga_location: side-navigation
      - text: Funzionalità Premium
        href: "#wp-premium-features"
        data_ga_name: premium-features
        data_ga_location: side-navigation
        nodes:
        - text: Revisioni del codice più veloci
          href: "#wp-faster-code-reviews"
          data_ga_name: faster-code-reviews
          data_ga_location: side-navigation
        - text: CI/CD avanzata
          href: "#wp-advanced-ci-cd"
          data_ga_name: advanced-ci-cd
          data_ga_location: side-navigation
        - text: Enterprise Agile Delivery
          href: "#wp-enterprise-agile-planning"
          data_ga_name: nterprise-agile-planning
          data_ga_location: side-navigation
        - text: Controlli di rilascio
          href: "#wp-release-controls"
          data_ga_name: release-controls
          data_ga_location: side-navigation
        - text: Affidabilità self-managed
          href: "#wp-self-managed-reliability"
          data_ga_name: self-managed-reliability
          data_ga_location: side-navigation
        - text: Altre funzionalità Premium
          href: "#wp-other-premium-features"
          data_ga_name: other-premium-features
          data_ga_location: side-navigation
      - text: Contattaci
        href: "#get-in-touch"
        data_ga_name: get-in-touch
        data_ga_location: side-navigation
    hyperlinks:
      text: Passaggi successivi
      data:
      - text: Acquista subito Premium
        href: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
        variant: primary
        icon: false
        data_ga_name: buy-premium
        data_ga_location: side-navigation
      - text: Scopri di più su Ultimate
        href: /pricing/ultimate/
        variant: secondary
        icon: true
        data_ga_name: learn-about-ultimate
        data_ga_location: side-navigation
  components:
  - name: plan-summary
    data:
      id: wp-summary
      title: Perché passare a Premium?
      subtitle: Ideale per migliorare la produttività e il coordinamento dei team
      text: "Disponibile sia in modalità SaaS che con deployment self-managed, GitLab Premium aiuta a migliorare la produttività e la collaborazione dei team attraverso:"
      list: 
        - Revisioni del codice più veloci
        - Controlli di rilascio
        - Supporto prioritario
        - CI/CD avanzata
        - Assistenza per upgrade in tempo reale
        - Enterprise Agile Delivery
        - Technical Account Manager per clienti idonei
        - Alta disponibilità e ripristino di emergenza per istanze self-managed
      cta: 
        text: Confronta tutte le funzionalità
        url: /pricing/feature-comparison/
      buttons:
        - variant: primary
          icon: false
          text: Acquista subito Premium
          url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
          data_ga_name: buy-premium-now
          data_ga_location: body
        - variant: secondary
          icon: true
          text: Scopri di più su Ultimate
          url: /pricing/ultimate/
          data_ga_name: learn-about-ultimate
          data_ga_location: body
  - name: guest-calculator
    data:
      id: wp-guest-calculator
      title: Calcola il costo
      input_title: GitLab offre utenti ospiti gratuiti illimitati con i piani Ultimate
      caption: |
        *Tutti i piani sono fatturati annualmente. I prezzi indicati possono essere soggetti alle imposte locali e alle ritenute d'acconto applicabili. I prezzi possono variare se si acquista tramite un partner o un rivenditore. Consulta la nostra [Pagina dei prezzi](/pricing/){data-ga-name="pricing" data-ga-location="guest calculator"} per ulteriori dettagli.
      seats:
        title: Numero di utenze di GitLab
        tooltip: Le utenze di GitLab sono persone con accesso in qualità di reporter, sviluppatore, gestore o proprietario. In GitLab Ultimate gli utenti ospiti non consumano utenze. <a href="https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined" data-ga-name="seat usage documentation" data-ga-location="user calculator tooltip">Qui</a> puoi trovare ulteriori informazioni sull'utilizzo delle utenze.
      guests:
        title: Numero di utenti ospiti
        tooltip: Gli utenti ospiti possono creare e assegnare ticket, visualizzare determinate analisi, visualizzare il codice (facoltativo) e altro ancora. Scopri di più <a href="https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions" data-ga-name="project members documentation" data-ga-location="user calculator tooltip">su questa pagina</a>.
      premium:
        title: Costo mensile di Premium*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
        data_ga_name: buy premium
        data_ga_location: guest calculator
        button: Acquista Premium
      ultimate:
        title: Costo mensile di Ultimate*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        data_ga_name: buy ultimate
        data_ga_location: guest calculator
        button: Acquista Ultimate
  - name: 'by-solution-value-prop'
    data:
      id: wp-key-solutions
      light_background: true
      small_margin: true
      large_card_on_bottom: true
      title: Con GitLab Premium puoi
      cards:
        - icon:
            name: increase
            alt: Icona di aumento
            variant: marketing
          title: Aumentare l'efficienza operativa
          description: GitLab Premium permette di analizzare le tendenze di team, progetti e gruppi con l'obiettivo di scoprire pattern e impostare standard coerenti per migliorare la produttività complessiva.
        - icon:
            name: speed-alt
            variant: marketing
            alt: Icona di velocità
          title: Offrire prodotti migliori in meno tempo
          description: Con CI/CD avanzate e revisioni del codice più veloci, GitLab Premium ti aiuta a sviluppare, gestire, implementare e monitorare meglio le pipeline di applicazioni complesse per distribuire prodotti più velocemente.
        - icon:
            name: lock-alt-5
            alt: Icona del lucchetto
            variant: marketing
          title: Mitigare i rischi di sicurezza e conformità
          description: Grazie ai controlli di rilascio di GitLab Premium, i team possono distribuire codice sicuro e di alta qualità.
  - name: 'education-case-study-carousel'
    data:
      id: 'wp-customer-case-studies'
      case_studies:
        - logo_url: /nuxt-images/logos/nvidia-logo.svg
          text: L'innovazione di NVIDIA supportata da GitLab Geo
          img_url: /nuxt-images/blogimages/nvidia.jpg
          case_study_url: /customers/nvidia/
          data_ga_name: Nvidia case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/case-study-logos/FujitsuCTL_Logo.png
          img_url: /nuxt-images/blogimages/fujitsu.png
          text: Fujitsu Cloud Technologies migliora la velocità di deployment e i flussi di lavoro tra le funzioni aziendali grazie a GitLab
          case_study_url: /customers/fujitsu/
          data_ga_name: fujitsu case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/customers/credit-agricole-logo-1.png
          img_url: /nuxt-images/blogimages/creditagricole-cover-image.jpg
          text: Crédit Agricole Corporate & Investment Bank (CACIB) trasforma il suo flusso di lavoro globale con GitLab
          case_study_url: /customers/credit-agricole
          data_ga_name: credit-agricole case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/esa-logo.svg
          img_url: /nuxt-images/blogimages/ESA_case_study_image.jpg
          text: L'Agenzia Spaziale Europea usa GitLab per le missioni spaziali
          case_study_url: /customers/european-space-agency
          data_ga_name: european-space-agency case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/sopra_steria-logo.wine.svg
          img_url: /nuxt-images/blogimages/soprasteria.jpg
          text: Come GitLab è diventato il cuore dell'abilitazione digitale per Sopra Steria
          case_study_url: /customers/sopra_steria
          data_ga_name: sopra_steria case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/customers/hemmersbach_logo.svg
          img_url: /nuxt-images/blogimages/hemmersbach_case_study.jpg
          text: Hemmersbach ha riorganizzato la catena di sviluppo e ha aumentato di 59 volte la velocità di compilazione
          case_study_url: /customers/hemmersbach
          data_ga_name: hemmersbach case study
          data_ga_location: case study carousel
  - name: roi-calculator-block
    data:
      id: wp-roi-calculator
      header:
        gradient_line: true
        title:
          text: Calcolatore del ROI
          anchor: wp-roi-calculator
        subtitle: Quanto ti costa la tua toolchain?
      calc_data_src: it-it/calculator/index
  features_block:
    id: wp-premium-features
    tier: premium
    header:
      gradient_line: true
      title:
        text: Funzionalità Premium
        anchor: wp-premium-features
        button:
          text: Confronta tutte le funzionalità
          data_ga_name: Compare all features
          data_ga_location: body
          url: /it-it/pricing/feature-comparison/
    pricing_themes:
      - id: wp-faster-code-reviews
        theme: Le revisioni del codice più veloci
        text: garantiscono un'elevata qualità del codice tra i team grazie a flussi di revisione fluidi.
        link:
          text: Scopri di più
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/development/code_review.html
      - id: wp-advanced-ci-cd
        theme: La CI/CD avanzata
        text: consente di sviluppare, gestire, implementare e monitorare pipeline complesse.
        link:
          text: Scopri di più
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: /solutions/continuous-integration/
      - id: wp-enterprise-agile-planning
        theme: L'Enterprise Agile Delivery
        text: ti aiuta a pianificare e gestire progetti, programmi e prodotti con il supporto Agile integrato.
        link:
          text: Scopri di più
          data_ga_name: Agile Planning learn more
          data_ga_location: body
          url: /solutions/agile-delivery/
      - id: wp-release-controls
        theme: I controlli di rilascio
        text: assicurano il deployment di codice sicuro e di alta qualità.
        link:
          text: Scopri di più
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
      - id: wp-self-managed-reliability
        theme: L'affidabilità self-managed
        text: garantisce il ripristino di emergenza, l'elevata disponibilità e il bilanciamento del carico del deployment self-managed.
        link:
          text: Scopri di più
          data_ga_name: Premium features learn more
          data_ga_location: body
          url: https://docs.gitlab.com/ee/administration/reference_architectures/#traffic-load-balancer
      - id: wp-other-premium-features
        text: Altre funzionalità Premium