title: Accelerate development with GitLab Duo Pro
og_title: Accelerate development with GitLab Duo Pro
description: >-
  Supercharge your coding with genAI. Protect your source code, IDE integration,
  multilingual support, and more! Get started for just $9/month. Contact us now.
twitter_description: >-
  Supercharge your coding with genAI. Protect your source code, IDE integration,
  multilingual support, and more! Get started for just $9/month. Contact us now.
image_title: >-
  //images.ctfassets.net/xz1dnu24egyd/6nDHWF2byPjkFySXitFst4/c5d149d864207a3b94151d70e5ba0ac7/GitLab-Duo-Code-Suggestions.jpg
og_description: >-
  Supercharge your coding with genAI. Protect your source code, IDE integration,
  multilingual support, and more! Get started for just $9/month. Contact us now.
og_image: >-
  //images.ctfassets.net/xz1dnu24egyd/6nDHWF2byPjkFySXitFst4/c5d149d864207a3b94151d70e5ba0ac7/GitLab-Duo-Code-Suggestions.jpg
twitter_image: >-
  //images.ctfassets.net/xz1dnu24egyd/6nDHWF2byPjkFySXitFst4/c5d149d864207a3b94151d70e5ba0ac7/GitLab-Duo-Code-Suggestions.jpg
aiContent:
  aiCard:
    eyebrow: Agentic AI
    title: Enterprise-grade, secure agentic AI
    body: >-
      Agentic AI is transforming software development, enabling developers to create software at a scale previously unimaginable — but it’s also raising important questions about visibility and security. The GitLab DevSecOps platform brings everything together so you can accelerate your development process while maintaining security and control.
  aiAccordion:
  - header: Secure agentic AI at scale
    icon:
      name: ai-vulnerability-resolution
      variant: marketing
      color: '#fff'
    text: >-
     Move beyond code assistance and transform the entire software development experience — from project bootstrapping to deployment processes — with agentic AI built on top of the most comprehensive DevSecOps platform.
    link_url: "http://about.gitlab.com/blog/2025/02/24/gitlab-duo-workflow-enterprise-visibility-and-control-for-agentic-ai/"
    data_ga_name: secure agentic ai at scale
    data_ga_location: body
  - header: Context-aware AI agents
    icon:
      name: ai-git-suggestions
      variant: marketing
      color: '#fff'
    text: >-
      GitLab Duo Workflow will understand the full context of your software development lifecycle — from a developer's tasks in the IDE to documentation, merge requests, and more.
    link_url: "http://about.gitlab.com/blog/2025/02/24/gitlab-duo-workflow-enterprise-visibility-and-control-for-agentic-ai/"
    data_ga_name: context-aware ai agents
    data_ga_location: body
  - header: Enterprise-grade control and security
    icon:
      name: eye
      variant: marketing
      color: '#fff'
    text: >-
      GitLab Duo Workflow leverages the unique benefits of the most comprehensive DevSecOps platform, including enterprise-grade safety, security, and governance with organizational control guardrails.
    link_url: "http://about.gitlab.com/blog/2025/02/24/gitlab-duo-workflow-enterprise-visibility-and-control-for-agentic-ai/"
    data_ga_name: enterprise-grade control and security
    data_ga_location: body
cards:
  - description: >-
      ##### Introducing GitLab Duo Workflow, the future of secure agentic AI software development.


      Built on the most comprehensive DevSecOps platform, GitLab Duo Workflow understands your entire software development lifecycle and maintains enterprise-grade security and control.


      **GitLab Duo Workflow accelerates how developers:**
      
      - Bootstrap new code projects

      - Modernize code

      - Perform contextual tasks

      - Create documentation

      - Enhance test coverage

      - and much more


      Sign up for the private beta waitlist to see what’s possible with AI agents that understand your entire SDLC.

    video: https://player.vimeo.com/video/1059060959?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=584798479
    variant: Default    
duoCombinedHero:
  title: The future of secure agentic AI software development
  primaryButton: 
    variant: tertiary
    data_ga_name: cta
    data_ga_location: hero
    text: Sign up now
    href: '#form'
    icon: 
      name: arrow-down
      variant: product

  blurb: Workflow
  id: gitlab-duo-combined-hero
  backgroundImage:
    src: >-
      //images.ctfassets.net/xz1dnu24egyd/2pLToUWGc7Vm7nnD3cuS30/674083fec72cf16a5867d8611032ba30/cb3747f9dbe2e63a8fe7d955f1e58f54.png
form:
  internalName: 28516 (pro form)
  formId: '28516'
  formDataLayer: sales
  registrationClosed: false
  blurb: >
    __Get on the list for the GitLab Duo Workflow beta__

    We’ll notify you as soon as you can join the GitLab Duo Workflow beta program.
resources:
  data:
    title: What’s new in GitLab AI
    column_size: 4
    cards:
      - header: Stay up to date on AI/ML at GitLab
        icon:
          name: blog
          variant: marketing
          alt: blog icon
        image: >-
          //images.ctfassets.net/xz1dnu24egyd/7m9wdMydyUHx9TS6oMqWw3/7e0d0ff4efb8ba2191e9a7131c7007f7/gitlabduo.webp
        link_text: Read more
        href: /blog/categories/ai-ml/
        data_ga_name: gitlab-duo-chat-beta
        data_ga_location: cs sale body
        event_type: Blog
      - header: 'AI trends for 2025: Agentic AI, self-hosted models, and more'
        icon:
          name: blog
          variant: marketing
          alt: blog icon
        image: >-
          //images.ctfassets.net/xz1dnu24egyd/1JQaFtSzNn8wLh4oeCzO5t/15dc0323234e2e0fad1f9a7cc527b0f0/ai-fireside-chat_2__1_.png
        link_text: Read more
        href: /the-source/ai/ai-trends-for-2025-agentic-ai-self-hosted-models-and-more/
        data_ga_name: gitlab-ai-assisted-features
        data_ga_location: body
        event_type: Blog
      - header: Reducing software development complexity with AI
        icon:
          name: blog
          variant: marketing
          alt: blog icon
        image: >-
          //images.ctfassets.net/xz1dnu24egyd/6hNLYhTjD4iG3o5zco602y/bdcbac01525ba265578625d65a4620f8/ai-experiment-stars.png
        link_text: Read more
        href: /the-source/ai/ai-trends-for-2025-agentic-ai-self-hosted-models-and-more/
        data_ga_name: ai-assisted-code-suggestions
        event_type: Blog
submittedHero:
  title: Thanks for reaching out!
  description: We'll be in touch soon to get you started with GitLab Duo Workflow.
  id: thank-you-hero
  backgroundImage:
    src: >-
      //images.ctfassets.net/xz1dnu24egyd/6Wyo8uOL4oJC7UI288hGNF/667cb43e43cf809e7f5acd33b40eda72/thankyou-stars-v2.svg
  icon: slp-support
disclaimer: This page describes upcoming product features for informational purposes only and should not be relied upon for purchasing or planning. Information is subject to change.
