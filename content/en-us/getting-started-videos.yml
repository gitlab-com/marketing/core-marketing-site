---
  title: Videos on GitLab best practices
  description: "See how GitLab employees use GitLab — and get best practices to take back to your team — in these short videos."
  components:
    - name: 'solutions-hero'
      data:
        title: Videos on GitLab best practices
        subtitle: See how GitLab employees use GitLab — and get best practices to take back to your team — in these short videos.
        aos_animation: fade-down
        aos_duration: 500
        title_variant: 'heading1-bold'
        mobile_title_variant: 'heading1-bold'
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
          text: Start your free trial
          data_ga_name: Start your free trial
          data_ga_location: header
        image:
          image_url: /nuxt-images/getting-started-videos/getting-started-videos_image.png
          alt: "Image: gitlab for open source"
          bordered: true

  videos:
    - title: How-to break down an initiative or large feature
      id: how-to-break-down-an-initiative-or-large-feature
      url: 'https://player.vimeo.com/video/897291697?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to find features to schedule
      id: how-to-find-features-to-schedule
      url: 'https://player.vimeo.com/video/897292136?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to plan the next milestone
      id: how-to-plan-the-next-milestone
      url: 'https://player.vimeo.com/video/897293166?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to organize my to-dos
      id: how-to-organize-my-to-dos
      url: 'https://player.vimeo.com/video/897290670?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to review issue progress
      id: how-to-review-issue-progress
      url: 'https://player.vimeo.com/video/897291029?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to promote an epic into an issue
      id: how-to-promote-an-epic-into-an-issue
      url: 'https://player.vimeo.com/video/897292524?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to do opportunity mapping
      id: how-to-do-opportunity-mapping
      url: 'https://player.vimeo.com/video/897289330?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to scope label use-cases
      id: how-to-scope-label-use-cases
      url: 'https://player.vimeo.com/video/897287874?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to plan epic start and due dates
      id: how-to-plan-epic-start-and-due-dates
      url: 'https://player.vimeo.com/video/897291930?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to bulk edit issues for an epic
      id: how-to-bulk-edit-issues-for-an-epic
      url: 'https://player.vimeo.com/video/897292804?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to use labels as a board column
      id: how-to-use-labels-as-a-board-column
      url: 'https://player.vimeo.com/video/897288649?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
    - title: How-to use an epic board to create a roadmap
      id: how-to-use-an-epic-board-to-create-a-roadmap
      url: 'https://player.vimeo.com/video/897293300?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
  button:
    text: Start your free trial
    href: /free-trial/
    data_ga_name: start your free trial
    data_ga_location: sidenav
  card:
    data:
      title: 'GitLab resources'
      description: "All the resources you need to take your idea to production. Learn Git, improve your DevSecOps workflow, or discover new trends with GitLab."
      link: /resources/
      image: /nuxt-images/getting-started-videos/getting-started-videos-discover.png
      icon: case-study-alt
      button_text: 'Discover GitLab Resources'
      alt: people during a meeting
      size: half
      button_type: primary
      image_right: true
  side_menu:
    anchors:
      text: "GitLab best practices"
      data:
        - text: Break down a large feature
          href: "#how-to-break-down-an-initiative-or-large-feature"
          data_ga_name: break down a large feature
          data_ga_location: side-navigation
          variant: primary
        - text: Find features to schedule
          href: "#how-to-find-features-to-schedule"
          data_ga_name: find features to schedule
          data_ga_location: side-navigation
        - text: Plan the next milestone
          href: "#how-to-plan-the-next-milestone"
          data_ga_name: plan the next milestone
          data_ga_location: side-navigation
        - text: Organize my to-dos
          href: "#how-to-organize-my-to-dos"
          data_ga_name: organize my to-dos
          data_ga_location: side-navigation
        - text: Review issue progress
          href: "#how-to-review-issue-progress"
          data_ga_name: review issue progress
          data_ga_location: side-navigation
          variant: primary
        - text: Promote an issue to an epic
          href: "#how-to-promote-an-epic-into-an-issue"
          data_ga_name: promote an issue to an epic
          data_ga_location: side-navigation
        - text: Opportunity mapping
          href: "#how-to-do-opportunity-mapping"
          data_ga_name: opportunity mapping
          data_ga_location: side-navigation
        - text: Scope label use cases
          href: "#how-to-scope-label-use-cases"
          data_ga_name: scope label use cases
          data_ga_location: side-navigation
        - text: Plan epic start and due dates
          href: "#how-to-plan-epic-start-and-due-dates"
          data_ga_name: plan epic start and due dates
          data_ga_location: side-navigation
          variant: primary
        - text: Bulk edit issues for an epic
          href: "#how-to-bulk-edit-issues-for-an-epic"
          data_ga_name: bulk edit issues for an epic
          data_ga_location: side-navigation
        - text: Use labels as a board column
          href: "#how-to-use-labels-as-a-board-column"
          data_ga_name: use labels as a board column
          data_ga_location: side-navigation
        - text: Use board to create roadmap
          href: "#how-to-use-an-epic-board-to-create-a-roadmap"
          data_ga_name: use board to create roadmap
          data_ga_location: side-navigation
    hyperlinks:
      data:
        - text: Request Services
          href: "#contactform"
          variant: primary
          data_ga_name: request services
          data_ga_location: side-navigation
        - text: Watch a demo
          modal: true
          icon:
            name: play
            variant: product
          variant: tertiary
          data_ga_name: learn-about-ultimate
          data_ga_location: side-navigation
