---
  title: "Migrate from Atlassian to GitLab"
  description: Atlassian is ending support of all Server products in February 2024, including Bitbucket, Jira, Bamboo, and Confluence. Don't let this change force you to adopt a tool that's not right for your team. Learn how GitLab can help.
  side_navigation_links:
    - title: Bitbucket
      href: '#bitbucket'
      data_ga_name: bitbucket
      data_ga_location: navigation
    - title: Jira
      href: '#jira'
      data_ga_name: jira
      data_ga_location: navigation
    - title: Bamboo
      href: '#bamboo'
      data_ga_name: bamboo
      data_ga_location: navigation
    - title: Confluence
      href: '#confluence'
      data_ga_name: confluence
      data_ga_location: navigation
    - title: Pricing
      href: '#pricing'
      data_ga_name: pricing
      data_ga_location: navigation
  side_navigation_text_link:
    text: Contact sales
    url: '/sales/'
    data_ga_name: sales
    data_ga_location: navigation
  solutions_hero:
    title: Migrate from Atlassian to GitLab
    badge:
      text: Enterprise Agile Planning add-on now available
      url: "#agile-add-on"
      icon: gl-arrow-right
      data_ga_name: agile delivery add-on
      data_ga_location: hero
    isDark: true
    subtitle: Atlassian is ending support for all Server products in **February 2024**, including [Bitbucket](#bitbucket), [Jira](#jira), [Bamboo](#bamboo), and [Confluence](#confluence). Don't let the Server end of life force you to adopt a tool that's not right for your team. Learn how GitLab can help.
    primary_btn:
      text: Talk to an expert
      url: /sales/
      data_ga_name: sales
      data_ga_location: header
    image:
      image_url: "https://images.ctfassets.net/xz1dnu24egyd/26SlAGBx32ZPYt2I4Wcd9A/95e7a4353d19da0b6a3eaff250e8205c/atlassian-eol-landing.svg"
      alt: "devops lifecycle image"
      bordered: true
  content_card:
    title: 'Move from Bitbucket to GitLab'
    description: |
      **Collaborate and accelerate**


      Always be launching with asset version control, feedback loops, and powerful branching patterns to help your developers efficiently solve problems and ship value.


      &nbsp;

      **Compliant and secure**


      Enable teams to review, track, and approve code changes with a single source of truth.
    link: /
    type: "eBook"
    image: https://images.ctfassets.net/xz1dnu24egyd/GF2chSP6KXImk3djWqJxk/d0119e7396e05f112e77583415405c10/principle-1.png
    icon: open-book
    button_text: 'Get the eBook'
    size: half
    button_type: primary
    image_right: true

  benefit_cards:
    title: GitLab is the fastest path from idea to software
    benefits:
      - title: Deploy anywhere
        icon: gitlab-cloud
        description: Choose how and where you want to deploy. GitLab will always serve customers that need a self-hosted solution.
      - title: Streamline software development
        icon: collaboration
        description: No more disjointed toolchains. Introduce your teams to a single platform for the entire software development lifecycle to shorten cycle times, boost productivity, and reduce development costs.
      - title: Go at your own pace
        icon: monitor-gitlab
        description: Not ready to transition away from all of your tools and services at once? No problem. GitLab also offers an extensive selection of integrations, so you can move at the speed that makes sense for your organization.

  copy_benefits_bitbucket:
    title: Move from Bitbucket to GitLab
    subtitle: Version control for everyone
    background: true
    benefits:
      - title: Powerful version control
        icon: idea-collaboration
        description: Always be launching with asset version control, feedback loops, and powerful branching patterns to help your developers efficiently test and deploy software.
      - title: Security and compliance, built in
        icon: devsecops
        description: Enable teams to review, track, and approve code changes with a single source of truth.
        button_text: Learn more
        button_url: /solutions/source-code-management/
        data_ga_name: bitbucket
        data_ga_location: body

  copy_benefits_jira:
    title: Move from Jira to GitLab
    subtitle: Integrated Agile support for projects, programs, and products
    benefits:
      - title: Agile planning
        icon: agile
        description: Accelerate business value delivery with out-of-the-box agile team and portfolio management features — all within the same software delivery platform that is used to build, test, and deploy software.
        button_text: Learn more
        button_url: /solutions/agile-delivery/
        data_ga_name: agile delivery
        data_ga_location: jira body
      - title: Value Stream Management
        icon: visibility
        description: Visualize value streams and eliminate bottlenecks across the entire software development lifecycle, reprioritize quickly to improve productivity, and drive business value.
        button_text: Learn more
        button_url: /solutions/value-stream-management/
        data_ga_name: value stream management
        data_ga_location: jira body

  copy_benefits_confluence:
    title: Move from Confluence to GitLab
    subtitle: Knowledge management to break down communication silos
    benefits:
      - title: Customizable wiki pages
        icon: web-alt
        description: Store organizational knowlege and documentation right within GitLab for easy access across teams and job functions.
        button_text: Learn more
        button_url: https://docs.gitlab.com/ee/user/project/wiki/
        data_ga_name: confluence
        data_ga_location: body

  copy_benefits_bamboo:
    title: Move from Bamboo to GitLab
    background: true
    subtitle: Flexible CI/CD to make software delivery scalable and on-demand
    benefits:
      - title: Pipelines built for simplicity and scale
        icon: monitor-pipeline
        description: Get started easily with built-in templates, and scale up with parent-child pipelines and merge trains.
      - title: Built-in security and compliance
        icon: shield-check-large-light
        description: From compliance pipelines to integrated security scanning, see it all in one place for better visibility and control.
        button_text: Learn more
        button_url: /solutions/security-compliance/
        data_ga_name: security compliance
        data_ga_location: bamboo body
      - title: In-context testing
        icon: monitor-test-2
        description: Test everything automatically, from code performance to security — and review and approve the results in-context.
        button_text: Learn more
        button_url: /solutions/continuous-integration/
        data_ga_name: bamboo
        data_ga_location: body

  resources:
    col_size: 4
    header: "Resources for migrating from Atlassian to GitLab"
    description: Learn more about Atlassian ending Server support and how to move to GitLab.
    case_studies:
      - header: "GitLab Enterprise Agile Planning add-on for all roles"
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/
          text: Learn more
          data_ga_name: enterprise agile planning resource
          data_ga_location: body
      - header: Why GitLab self-managed is the perfect partner for the public sector
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/3wDRL8ssQu72bNfsvN1Sds/1b48bc3c8cb63eca3f47cf40b99a6308/gitlabflatlogomap.jpg
          alt: ""
        link:
          href: /blog/2023/12/13/why-gitlab-self-managed-is-the-perfect-partner-for-the-public-sector/
          text: Learn more
          data_ga_name: perfect partner resource
          data_ga_location: body
      - header: "Atlassian Server ending: Goodbye disjointed toolchain, hello DevSecOps platform"
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/HE9XYEbagTAiFYXohe8p9/47f0ab25fda5c00db236e7bb91790492/value-stream.png
          alt: ""
        link:
          href: /blog/2023/09/26/atlassian-server-ending-move-to-a-single-devsecops-platform/
          text: Learn more
          data_ga_name: atlassian resource
          data_ga_location: body
      - header: 5 reasons to simplify your agile planning tool configuration with GitLab
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/10/17/five-reasons-to-simplify-agile-planning-tool-configuration-gitlab/
          text: Learn more
          data_ga_name: simplify agile resource
          data_ga_location: body
      - header: Tips for a successful Jira to GitLab migration
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/1tjLWk83Avww6hrBC1OESN/c2ab302916852afe2155e69fa21436f6/agile.png
          alt: ""
        link:
          href: /blog/2023/10/24/tips-for-a-successful-jira-to-gitlab-migration/
          text: Learn more
          data_ga_name: Tips for a successful Jira to GitLab migration resource
          data_ga_location: body
      - header: How to migrate from Bamboo to GitLab CI/CD
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/26eTPBKrWglfy1ILxWSUsD/7230b6d753d78b80abbcfe736449e218/securitylifecycle-light.png
          alt: ""
        link:
          href: /blog/2023/10/26/migrating-from-bamboo-to-gitlab-cicd/
          text: Learn more
          data_ga_name: How to migrate from Bamboo to GitLab CI/CD resource
          data_ga_location: body
      - header: Import your project from Bitbucket Cloud to GitLab
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/7veFR11eLgKvggQfNISDgK/d0ecb9098c5b913e22d90790d8cb7391/bitbucket.png
          alt: ""
        link:
          href: https://docs.gitlab.com/ee/user/project/import/bitbucket.html
          text: Learn more
          data_ga_name: bitbucket resource
          data_ga_location: body
      - header: Import your Jira project issues to GitLab
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/5mnlgW5gqy5JBvE819dH2q/25521343423e026ada32fbd7daf80b5a/jira.png
          alt: ""
        link:
          href: https://docs.gitlab.com/ee/user/project/import/jira.html
          text: Learn more
          data_ga_name: jira resource
          data_ga_location: body
      - header: 1 billion pipelines of CI/CD innovation
        img:
          url: https://images.ctfassets.net/xz1dnu24egyd/19z3IxRhCpQf37oD2lEOHN/34faaf9fb0fd927efe6c30a10f74c499/securitylifecycle.png
          alt: ""
        link:
          href: /blog/2023/10/04/one-billion-pipelines-cicd/
          text: Learn more
          data_ga_name: pipeline resource
          data_ga_location: body
  tier_block:
    header: Which tier is right for you?
    cta:
      url: /pricing/
      text: Which tier is right for you?
      data_ga_name: pricing
      data_ga_location: free tier
      aria_label: pricing
    tiers:
      - id: free
        title: Free
        items:
          - Static application security testing (SAST) and secrets detection
          - Findings in json file
        link:
          href: /pricing/
          text: Learn more
          data_ga_name: pricing
          data_ga_location: free tier
          aria_label: free tier
      - id: premium
        title: Premium
        items:
          - Static application security testing (SAST) and secrets detection
          - Findings in json file
          - MR approvals and more common controls
        link:
          href: /pricing/
          text: Learn more
          data_ga_name: pricing
          data_ga_location: premium tier
          aria_label: premium tier
      - id: ultimate
        title: Ultimate
        items:
          - Everything in Premium plus
          - Comprehensive security scanners include SAST, DAST, Secrets, dependencies, containers, IaC, APIs, cluster images, and fuzz testing
          - Actionable results within the MR pipeline
          - Compliance pipelines
          - Security and Compliance dashboards
          - Much more
        link:
          href: /pricing/
          text: Learn more
          data_ga_name: pricing
          data_ga_location: ultimate tier
          aria_label: ultimate tier
        cta:
          href: /free-trial/
          text: Start your free trial
          data_ga_name: pricing
          data_ga_location: ultimate tier
          aria_label: ultimate tier

  pricing_banner:
    id: agile-add-on
    badge_text: New
    headline: GitLab Enterprise Agile Planning
    blurb: Additional Agile Planning seats for GitLab Ultimate customers.
    button:
      text: Contact us for pricing
      url: "/sales/"
      data_ga_name: contact us for pricing
      data_ga_location: body
    footnote: ''
    features:
      header: 'Enterprise-grade Agile Planning solution:'
      list:
        - text: Replacement for Jira
        - text: One planning workflow for everyone involved in the software development lifecycle
        - text: Value stream analytics to measure velocity and impact
        - text: Executive dashboards for organization-wide visibility
        - text: Stand-alone Enterprise Agile Planning seats for GitLab Ultimate customers


  recognition:
    heading: GitLab is the leading DevSecOps platform
    stats:
      - value: 50%+
        blurb: Fortune 100
      - value: 30m+
        blurb: Registered users
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: gartner logo
        text: 'GitLab is a Leader in the 2024 Gartner® Magic Quadrant™ for DevOps Platforms'
        link:
          text: Read the report
          href: /gartner-magic-quadrant/
          data_ga_name: gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: forrester logo
        text: "GitLab is the only Leader in The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
        link:
          text: Read the report
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
          data_ga_name: forrester
          data_ga_location: analyst
      - header: 'GitLab ranks as a G2 Leader across DevSecOps categories'
        link:
          text: What industry analysts are saying about GitLab
          href: /analysts/
          data_ga_name: g2
          data_ga_location: analyst
        badges:
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 Enterprise Leader - Summer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MomentumLeader_Leader.png
            alt: G2 Momentum Leader- Summer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MostImplementable_Total.svg
            alt: G2 Most Implementable - Summer 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestResults_Total.png
            alt: G2 Best Results - Summer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Enterprise_Total.png
            alt: G2 Best Relationship Enterprise - Summer 2023
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Mid-Market_Total.svg
            alt: G2 Best Relationship Mid-Market - Summer 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 Easiest to use - Summer 2023
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestUsability_Total.png
            alt: G2 Best Usability - Summer 2023

  atlassian_solutions:
    title: GitLab integrates with hundreds of existing applications
    subtitle: Interested in how your product and engineering teams can partner better without switching tools? [Contact us to learn more about custom solutions](/sales/).
    solutions:
      - title: Jira
        subtitle: Simple [Atlassian Jira integration](https://docs.gitlab.com/ee/integration/jira/){data-ga-name="jira docs" data-ga-location="body"}
      - title: GitHub
        subtitle: Seamless [GitLab CI/CD with GitHub SCM](https://docs.gitlab.com/ee/user/project/integrations/github.html){data-ga-name="github docs" data-ga-location="body"}
      - title: Jenkins
        subtitle: Well-maintained [GitLab plugin](https://docs.gitlab.com/ee/integration/jenkins.html){data-ga-name="jenkins docs" data-ga-location="body"}
      - title: APIs
        subtitle: '[APIs](https://docs.gitlab.com/ee/api/integrations.html){data-ga-name="apis" data-ga-location="body"} on every GitLab component'
