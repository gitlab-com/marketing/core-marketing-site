import Vue from 'vue';
import { UseLocalizedVideoSubtitles } from '~/mixins/use-localized-video-subtitles.mixin';

export const UseLiteYouTube = Vue.extend({
  mixins: [UseLocalizedVideoSubtitles],
  methods: {
    // Function to extract the YouTube video ID from the URL
    /* getYTVideoId method to extract the YouTube video ID from the URL
     only handles urls of the following format:
     https://www.youtube.com/watch?v=VIDEO_ID
     https://youtu.be/VIDEO_ID
     https://www.youtube.com/embed/VIDEO_ID
     https://www.youtube.com/watch?v=VIDEO_ID&list=XYZ */
    getYTVideoId(url) {
      if (!url) {
        return null;
      }
      const regex =
        /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/i;
      const match = url.match(regex);
      return match && match[1] ? match[1] : null;
    },

    // Initialize the YouTube player (no need to manually create iframe) - different from lite-vimeo
    initializeYouTubePlayers() {
      this.$nextTick(() => {
        const liteYouTubeElements = document.querySelectorAll('lite-youtube');
        if (liteYouTubeElements.length === 0) {
          return;
        }

        for (let liteYouTube of liteYouTubeElements) {
          // Prevent double initialization by checking for the 'lyt-activated' class
          if (!liteYouTube.classList.contains('lyt-activated')) {
            const videoId = liteYouTube.getAttribute('videoid');
            const videoTitle =
              liteYouTube.getAttribute('title') || `Video ${videoId}`;
            const eventLabel = `${videoTitle} | ${videoId}`;

            // Add event listener to activate the YouTube iframe on interaction
            liteYouTube.addEventListener('click', () => {
              if (!liteYouTube.classList.contains('lyt-activated')) {
                liteYouTube.activate(); // Use the built-in activate function to load the iframe
              }
            });
          }
        }
      });
    },
  },

  // Initialize YouTube players when the component is mounted
  mounted() {
    this.initializeYouTubePlayers();
  },
});
