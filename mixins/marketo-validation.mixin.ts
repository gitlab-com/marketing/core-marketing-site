import Vue from 'vue';
import { validateMarketoForm } from '~/common/validation';

export const marketoValidationMixin = Vue.extend({
  computed: {
    formName() {
      return (
        this.data.datalayer ||
        this.data.formDataLayer ||
        this.data.dataLayer ||
        'mktoFormFill_uncaught'
      );
    },
  },
  created() {
    if (!this.data.external_form) {
      validateMarketoForm(this.data, this.$route.path);
    }
  },
});
