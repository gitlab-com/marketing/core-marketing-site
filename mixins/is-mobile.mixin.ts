import Vue from 'vue';

export const IsMobileMixin = Vue.extend({
  data() {
    return {
      isMobile: false,
      width: 0,
      breakpoint: 769,
    };
  },
  mounted() {
    this.onResize();
    window.addEventListener('resize', this.onResize);
  },
  destroyed() {
    window.removeEventListener('resize', this.onResize);
  },
  methods: {
    onResize() {
      this.isMobile = window.innerWidth < this.breakpoint;
      this.width = window.innerWidth;
    },
  },
});
