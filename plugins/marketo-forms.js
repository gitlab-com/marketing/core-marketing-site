/**
 * Use this plugin to configure anything related to Marketo Forms globally
 * This will work only on client-side
 */

import marketoTranslations from '@/plugins/marketo-translations.js';

export default function () {
  if (window.MktoForms2) {
    window.MktoForms2.whenReady(function (form) {
      if (typeof gtag !== 'undefined') {
        gtag('get', 'G-ENFH3X7M5Y', 'client_id', function (clientId) {
          const clientIdInput = document.getElementsByName('GACLIENTID__c')[0];
          if (clientIdInput) {
            clientIdInput.value = clientId;
          }
        });

        const trackingId = 'G-ENFH3X7M5Y';
        const trackIdInput = document.getElementsByName('GATRACKID__c')[0];
        if (trackIdInput) {
          trackIdInput.value = trackingId;
        }
      }
      marketoTranslations();
    });
  }
}
