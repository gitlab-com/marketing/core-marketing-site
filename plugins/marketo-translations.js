export default function marketoTranslations() {
  const translations = {
    fr: [
      { mktoFieldId: 'FirstName', label: 'Prénom' },
      { mktoFieldId: 'LastName', label: 'Nom' },
      { mktoFieldId: 'Email', label: 'Adresse mail professionnelle' },
      {
        mktoFieldId: 'InstructEmail',
        label:
          "Cet e-mail doit correspondre au contact de l'abonnement pour le compte.",
      },
      { mktoFieldId: 'gitLabSubscriptionName', label: "Nom de l'abonnement" },
      {
        mktoFieldId: 'instructionText',
        label:
          "Vous pouvez le retrouver en cliquant sur le lien contenu dans l'e-mail que vous avez reçu après votre achat ou sur la <a href='https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html#on-self-managed-and-gitlab-dedicated' target='_blank'>page Abonnements et achats du portail clients GitLab</a>. Le format A-S00123456 est souvent utilisé.",
      },
      { mktoFieldId: 'Company', label: 'Nom de l’entreprise' },
      { mktoFieldId: 'Phone', label: 'Tél. professionnel' },
      { mktoFieldId: 'Country', label: 'Pays' },
      { mktoFieldId: 'City', label: 'Ville' },
      { mktoFieldId: 'State', label: 'État' },
      { mktoFieldId: 'commentCapture', label: 'Avez-vous des questions ?' },
      {
        mktoFieldId: 'Non_Crimean',
        label:
          'Je confirme que je ne vis pas dans la région de Crimée en Ukraine.',
      },
      {
        mktoFieldId: 'optIn',
        label:
          "J'accepte que GitLab me contacte par e-mail et par téléphone au sujet de ses produits, services et événements.",
        description: 'checkbox',
      },
      {
        mktoFieldId: 'optinPlaceholder',
        label:
          "J'accepte que GitLab me contacte par e-mail et par téléphone au sujet de ses produits, services et événements.",
        description: 'checkbox',
      },
      {
        mktoFieldId: 'mkto_consent',
        label:
          "En indiquant vos coordonnées, vous acceptez que GitLab vous contacte par e-mail et par téléphone à propos de ses produits, services et événements. Vous pouvez retirer votre consentement à tout moment en vous désabonnant des e-mails ou en vous rendant sur notre <a href='https://about.gitlab.com/fr-fr/company/preference-center/'>centre de préférences de communication</a>.",
      },
      {
        mktoFieldId: 'mkto_subscription',
        label:
          "En cliquant sur « Commencer », vous acceptez le <a rel='nofollow noreferrer noopener' href='https://handbook.gitlab.com/handbook/legal/subscription-agreement/' target='_blank'>Contrat d'abonnement GitLab</a> et reconnaissez avoir pris connaissance de notre <a rel='nofollow noreferrer noopener' href='https://about.gitlab.com/privacy/' target='_blank'>Politique de confidentialité</a>. Pour fournir cette fonctionnalité, GitLab envoie des données de l'instance, y compris des données personnelles, à nos <a rel='nofollow noreferrer noopener' href='https://docs.gitlab.com/ee/user/ai_features.html#data-usage' target='_blank'>fournisseurs de solutions d'IA</a>.",
      },
      { mktoFieldId: 'submitButton', label: 'Soumettre' },
      { mktoFieldId: 'submitButtonRegister', label: "S'inscrire" },
      { mktoFieldId: 'submitButtonPleaseWait', label: 'Veuillez patienter' },
      { mktoFieldId: 'submitButtonGetInTouch', label: 'Prendre contact' },
      { mktoFieldId: 'submitButtonGetStarted', label: 'Commencer' },
    ],
    de: [
      { mktoFieldId: 'FirstName', label: 'Vorname' },
      { mktoFieldId: 'LastName', label: 'Nachname' },
      { mktoFieldId: 'Email', label: 'Geschäftliche E-Mail-Adresse' },
      {
        mktoFieldId: 'InstructEmail',
        label:
          'Diese E-Mail-Adresse muss mit dem Abonnementkontakt für das Konto übereinstimmen.',
      },
      { mktoFieldId: 'gitLabSubscriptionName', label: 'Abonnementname' },
      {
        mktoFieldId: 'instructionText',
        label:
          "Du kannst darauf über die E-Mail, die du nach dem Kauf erhalten hast, oder über die <a href='https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html#on-self-managed-and-gitlab-dedicated' target='_blank'>Seite „Abonnements und Käufe“ im Kundenportal</a> zugreifen. Es hat oft das Format A-S00123456.",
      },
      { mktoFieldId: 'Company', label: 'Firmenname' },
      { mktoFieldId: 'Phone', label: 'Geschäftliche Telefonnummer' },
      { mktoFieldId: 'Country', label: 'Land' },
      { mktoFieldId: 'City', label: 'Ort' },
      { mktoFieldId: 'State', label: 'Bundesstaat/Provinz' },
      { mktoFieldId: 'commentCapture', label: 'Hast du erste Fragen?' },
      {
        mktoFieldId: 'Non_Crimean',
        label:
          'Ich bestätige, dass ich nicht in der Krim-Region in der Ukraine lebe.',
      },
      {
        mktoFieldId: 'optIn',
        label:
          'Ich bin damit einverstanden, von GitLab per E-Mail und Telefon über Produkte, Dienstleistungen und Veranstaltungen informiert zu werden.',
        description: 'checkbox',
      },
      {
        mktoFieldId: 'optinPlaceholder',
        label:
          'Ich bin damit einverstanden, von GitLab per E-Mail und Telefon über Produkte, Dienstleistungen und Veranstaltungen informiert zu werden.',
        description: 'checkbox',
      },
      {
        mktoFieldId: 'mkto_consent',
        label:
          "Durch die Angabe Ihrer Kontaktinformationen erklären Sie sich damit einverstanden, dass GitLab Sie per E-Mail und Telefon über seine Produkte, Dienstleistungen und Veranstaltungen kontaktiert. Sie können sich jederzeit in E-Mails oder in unserem <a href='https://about.gitlab.com/de-de/company/preference-center/'>Communication-Preference-Center</a> abmelden.",
      },
      {
        mktoFieldId: 'mkto_subscription',
        label:
          "Indem du auf „Los geht’s“ klickst, akzeptierst du die <a rel='nofollow noreferrer noopener' href='https://handbook.gitlab.com/handbook/legal/subscription-agreement/' target='_blank'>GitLab-Abonnementvereinbarung</a> und stimmst unserer <a rel='nofollow noreferrer noopener' href='https://about.gitlab.com/privacy/' target='_blank'>Datenschutzerklärung</a> zu. Um diese Funktion bereitzustellen, sendet GitLab Daten aus der Instanz, einschließlich persönlicher Daten, an unsere <a rel='nofollow noreferrer noopener' href='https://docs.gitlab.com/ee/user/ai_features.html#data-usage' target='_blank'>KI-Anbieter</a>.",
      },
      { mktoFieldId: 'submitButton', label: 'Los geht’s' },
      { mktoFieldId: 'submitButtonPleaseWait', label: 'Warten Sie mal' },
      { mktoFieldId: 'submitButtonGetInTouch', label: 'Kontakt aufnehmen' },
      { mktoFieldId: 'submitButtonGetStarted', label: 'Beginnen' },
    ],

    ja: [
      { mktoFieldId: 'FirstName', label: '名' },
      { mktoFieldId: 'LastName', label: '姓' },
      { mktoFieldId: 'Email', label: '勤務先メールアドレス' },
      {
        mktoFieldId: 'InstructEmail',
        label:
          'メールアドレスは、アカウントのサブスクリプションで登録されている連絡先情報と一致している必要があります。',
      },
      { mktoFieldId: 'gitLabSubscriptionName', label: 'サブスクリプション名' },
      {
        mktoFieldId: 'instructionText',
        label:
          "これはご購入後に送信されるメール、またはカスタマーポータルの<a href='https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html#on-self-managed-and-gitlab-dedicated' target='_blank'>サブスクリプションと購入ページ</a>からアクセスできます。通常「A-S00123456」のような形式が使用されます。",
      },
      { mktoFieldId: 'Company', label: '会社名' },
      { mktoFieldId: 'Phone', label: '会社の電話番号（オプション)' },
      { mktoFieldId: 'Country', label: '国' },
      { mktoFieldId: 'City', label: '市区町村' },
      { mktoFieldId: 'State', label: '州／都道府県' },
      {
        mktoFieldId: 'commentCapture',
        label: '何かご不明な点はございますか？（オプション)',
      },
      {
        mktoFieldId: 'Non_Crimean',
        label: '私はウクライナのクリミアには住んでいません',
      },
      {
        mktoFieldId: 'optIn',
        label:
          '私は、GitLabから製品、サービス、イベントについてメールと電話での連絡を受け取ることに同意します。',
        description: 'checkbox',
      },
      {
        mktoFieldId: 'optinPlaceholder',
        label:
          '私は、GitLabから製品、サービス、イベントについてメールと電話での連絡を受け取ることに同意します。',
        description: 'checkbox',
      },
      {
        mktoFieldId: 'mkto_consent',
        label:
          "連絡先情報をご提供いただくことで、GitLabより製品、サービス、イベントに関する連絡をメールや電話でさせていただくことに同意いただいたこととします。お客様は、メールを送っていただくか、当社の<a href='https://about.gitlab.com/ja-jp/company/preference-center/'>メール受信設定画面</a>にアクセスすることで、いつでも配信を停止することができます。",
      },
      {
        mktoFieldId: 'mkto_subscription',
        label:
          "[開始する]をクリックすることで、<a rel='nofollow noreferrer noopener' href='https://handbook.gitlab.com/handbook/legal/subscription-agreement/' target='_blank'>GitLabサブスクリプション契約</a>および<a rel='nofollow noreferrer noopener' href='https://about.gitlab.com/privacy/' target='_blank'>プライバシーステートメント</a>に同意したものとみなされます。GitLabはデータをAIプロバイダーに送信します。",
      },
      { mktoFieldId: 'submitButton', label: '開始する' },
      { mktoFieldId: 'submitButtonPleaseWait', label: 'お待ちください' },
      { mktoFieldId: 'submitButtonGetInTouch', label: 'お問い合わせ' },
      { mktoFieldId: 'submitButtonGetStarted', label: 'スタート' },
    ],

    es: [
      { mktoFieldId: 'FirstName', label: 'Nombre' },
      { mktoFieldId: 'LastName', label: 'Apellido' },
      {
        mktoFieldId: 'Email',
        label: 'Dirección de correo electrónico laboral',
      },
      {
        mktoFieldId: 'InstructEmail',
        label:
          'Este correo electrónico debe coincidir con el contacto de suscripción de la cuenta.',
      },
      {
        mktoFieldId: 'gitLabSubscriptionName',
        label: 'Nombre de la suscripción',
      },
      {
        mktoFieldId: 'instructionText',
        label:
          "Puede acceder desde su correo electrónico posterior a la compra o en la <a href='https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html#on-self-managed-and-gitlab-dedicated' target='_blank'>página de suscripciones y compras en el Portal de clientes</a>. El formato suele ser A-S00123456.",
      },
      { mktoFieldId: 'Company', label: 'Nombre de la empresa' },
      { mktoFieldId: 'Phone', label: 'Número de teléfono del trabajo' },
      { mktoFieldId: 'Country', label: 'País/Región' },
      { mktoFieldId: 'City', label: 'Ciudad' },
      { mktoFieldId: 'State', label: 'Estado/Provincia' },
      {
        mktoFieldId: 'commentCapture',
        label: '¿Tiene preguntas antes de comenzar?',
      },
      {
        mktoFieldId: 'Non_Crimean',
        label: 'Confirmo que no vivo en la región de Crimea de Ucrania.',
      },
      {
        mktoFieldId: 'optIn',
        label:
          'Acepto que GitLab pueda contactarme por correo electrónico y teléfono sobre sus productos, servicios y eventos.',
        description: 'checkbox',
      },
      {
        mktoFieldId: 'optinPlaceholder',
        label:
          'Acepto que GitLab pueda contactarme por correo electrónico y teléfono sobre sus productos, servicios y eventos.',
        description: 'checkbox',
      },
      {
        mktoFieldId: 'mkto_consent',
        label:
          "Al registrarte en este evento, aceptas que Gitlab pueda enviarte correos electrónicos sobre sus productos, servicios y eventos. Puedes cancelar la suscripción en cualquier momento desde los correos electrónicos o visitando nuestro <a href='https://about.gitlab.com/company/preference-center/'>centro de preferencias de comunicación</a>.",
      },
      {
        mktoFieldId: 'mkto_subscription',
        label:
          "Al hacer clic en 'Comenzar', acepta el <a rel='nofollow noreferrer noopener' href='https://handbook.gitlab.com/handbook/legal/subscription-agreement/' target='_blank'>Contrato de suscripción a GitLab</a> y confirma la recepción de nuestra <a rel='nofollow noreferrer noopener' href='https://about.gitlab.com/privacy/' target='_blank'>Declaración de privacidad</a>. Para proporcionar esta funcionalidad, GitLab enviará datos de la instancia, incluidos datos personales, a nuestros <a rel='nofollow noreferrer noopener' href='https://docs.gitlab.com/ee/user/ai_features.html#data-usage' target='_blank'>proveedores de IA</a>.",
      },
      { mktoFieldId: 'submitButton', label: 'Comenzar' },
      { mktoFieldId: 'submitButtonPleaseWait', label: 'Espere' },
      { mktoFieldId: 'submitButtonGetInTouch', label: 'Ponte en contacto' },
      { mktoFieldId: 'submitButtonGetStarted', label: 'Empezar' },
    ],
    pt: [
      { mktoFieldId: 'FirstName', label: 'Nome' },
      { mktoFieldId: 'LastName', label: 'Sobrenome' },
      { mktoFieldId: 'Email', label: 'E-mail profissional' },
      {
        mktoFieldId: 'InstructEmail',
        label:
          'Este e-mail deve corresponder ao contato de assinatura da conta.',
      },
      { mktoFieldId: 'gitLabSubscriptionName', label: 'Nome da assinatura' },
      {
        mktoFieldId: 'instructionText',
        label:
          "Você encontra essa informação no e-mail de pós-compra ou na <a href='https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html#on-self-managed-and-gitlab-dedicated' target='_blank'>página de Assinaturas e compras no Portal de clientes</a>. Geralmente tem o formato A-S00123456.",
      },
      { mktoFieldId: 'Company', label: 'Nome da empresa' },
      { mktoFieldId: 'Phone', label: 'Telefone comercial' },
      { mktoFieldId: 'Country', label: 'País/Região' },
      { mktoFieldId: 'City', label: 'Cidade' },
      { mktoFieldId: 'State', label: 'Estado/Província' },
      { mktoFieldId: 'commentCapture', label: 'Tem alguma dúvida inicial?' },
      {
        mktoFieldId: 'Non_Crimean',
        label: 'Confirmo que não moro na região da Crimeia, na Ucrânia.',
      },
      {
        mktoFieldId: 'optIn',
        label:
          'Concordo que o GitLab pode entrar em contato comigo por e-mail e telefone para falar de seus produtos, serviços e eventos.',
        description: 'checkbox',
      },
      {
        mktoFieldId: 'optinPlaceholder',
        label:
          'Concordo que o GitLab pode entrar em contato comigo por e-mail e telefone para falar de seus produtos, serviços e eventos.',
        description: 'checkbox',
      },
      {
        mktoFieldId: 'mkto_consent',
        label:
          "Ao se inscrever para esse evento, você concorda em receber e-mails sobre produtos, serviços e eventos da GitLab. Para deixar de recebê-los, você pode cancelar a inscrição nos e-mails ou acessar nosso <a href='https://about.gitlab.com/company/preference-center/'>centro de preferências de comunicação</a> quando quiser.",
      },
      {
        mktoFieldId: 'mkto_subscription',
        label:
          "Ao clicar em 'Cadastrar-se', você aceita o <a rel='nofollow noreferrer noopener' href='https://handbook.gitlab.com/handbook/legal/subscription-agreement/' target='_blank'>Contrato de Assinatura do GitLab</a> e confirma estar ciente da nossa <a rel='nofollow noreferrer noopener' href='https://about.gitlab.com/privacy/' target='_blank'>Declaração de Privacidade</a>. Para oferecer esse recurso, o GitLab enviará dados da instância, incluindo dados pessoais, para nossos <a rel='nofollow noreferrer noopener' href='https://docs.gitlab.com/ee/user/ai_features.html#data-usage' target='_blank'>provedores de IA</a>.",
      },
      { mktoFieldId: 'submitButton', label: 'Cadastrar-se' },
      { mktoFieldId: 'submitButtonPleaseWait', label: 'Por favor, aguarde' },
      { mktoFieldId: 'submitButtonGetInTouch', label: 'Entre em contato' },
      { mktoFieldId: 'submitButtonGetStarted', label: 'Começar' },
    ],

    it: [
      { mktoFieldId: 'FirstName', label: 'Nome' },
      { mktoFieldId: 'LastName', label: 'Cognome' },
      { mktoFieldId: 'Email', label: 'Indirizzo email aziendale' },
      {
        mktoFieldId: 'InstructEmail',
        label:
          "Questo indirizzo email deve corrispondere a quello utilizzato per l'abbonamento dell'account.",
      },
      { mktoFieldId: 'gitLabSubscriptionName', label: 'Nome abbonamento' },
      {
        mktoFieldId: 'instructionText',
        label:
          "Puoi accedervi dall'email che ricevi dopo l'acquisto o dalla <a href='https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html#on-self-managed-and-gitlab-dedicated' target='_blank'>pagina Abbonamenti e acquisti nel Portale clienti</a>. Il formato frequente è A-S00123456.",
      },
      { mktoFieldId: 'Company', label: 'Nome azienda' },
      { mktoFieldId: 'Phone', label: 'Numero di telefono aziendale' },
      { mktoFieldId: 'Country', label: 'Paese/Area geografica' },
      { mktoFieldId: 'City', label: 'Città' },
      { mktoFieldId: 'State', label: 'Stato/Provincia' },
      {
        mktoFieldId: 'commentCapture',
        label: "C'è qualcosa che vorresti sapere prima di iniziare?",
      },
      {
        mktoFieldId: 'Non_Crimean',
        label: 'Confermo di non vivere nella regione ucraina della Crimea.',
      },
      {
        mktoFieldId: 'optIn',
        label:
          'Accetto che GitLab possa contattarmi via email e telefono in merito ai suoi prodotti, servizi ed eventi.',
        description: 'checkbox',
      },
      {
        mktoFieldId: 'optinPlaceholder',
        label:
          'Accetto che GitLab possa contattarmi via email e telefono in merito ai suoi prodotti, servizi ed eventi.',
        description: 'checkbox',
      },
      {
        mktoFieldId: 'mkto_consent',
        label:
          "Con l'iscrizione a questo evento, accetti che GitLab possa inviarti e-mail sui suoi prodotti, servizi ed eventi. Puoi annullare l'iscrizione in qualsiasi momento revocando il consenso alla ricezione dei messaggi di posta elettronica o visitando il nostro <a href='https://about.gitlab.com/company/preference-center/'>centro preferenze per le comunicazioni</a>.",
      },
      {
        mktoFieldId: 'mkto_subscription',
        label:
          "Facendo clic su 'Inizia ora', accetti il <a rel='nofollow noreferrer noopener' href='https://handbook.gitlab.com/handbook/legal/subscription-agreement/' target='_blank'>Contratto di abbonamento di GitLab</a> e confermi di aver preso visione della nostra <a rel='nofollow noreferrer noopener' href='https://about.gitlab.com/privacy/' target='_blank'>Informativa sulla privacy</a>. Per fornire questa funzionalità, GitLab invierà dati dell'istanza, inclusi i tuoi dati personali, ai nostri <a rel='nofollow noreferrer noopener' href='https://docs.gitlab.com/ee/user/ai_features.html#data-usage' target='_blank'>fornitori di IA</a>.",
      },
      { mktoFieldId: 'submitButton', label: 'Inizia ora' },
      { mktoFieldId: 'submitButtonPleaseWait', label: 'Attendi' },
      { mktoFieldId: 'submitButtonGetInTouch', label: 'Contattaci' },
      { mktoFieldId: 'submitButtonGetStarted', label: 'Iniziare' },
    ],
  };

  // Retrieve the language object from localStorage and parse it
  let currentLanguage = 'eng'; // Default to English
  try {
    const storedLanguage = localStorage.getItem('lang');
    currentLanguage = storedLanguage ? JSON.parse(storedLanguage).code : 'eng';
  } catch (error) {
    console.error(
      "Error parsing language from localStorage, defaulting to 'eng':",
      error,
    );
  }

  // Messages translation object for form validation messages
  const messages = {
    ja: [
      { search: 'This field is required.', replace: 'この項目は必須です。' },
      {
        search: 'Must be valid email.',
        replace: '有効なメールをご使用ください。',
      },
    ],
    it: [
      {
        search: 'This field is required.',
        replace: 'Questo campo è obbligatorio.',
      },
      {
        search: 'Must be valid email.',
        replace: "Deve essere un'e-mail valida.",
      },
    ],
    fr: [
      {
        search: 'This field is required.',
        replace: 'Ce champ est obligatoire.',
      },
      {
        search: 'Must be valid email.',
        replace:
          "Doit être un e-mail valide. <span class = 'mktoErrorDetail'> example@votredomaine.com </span>",
      },
    ],
    de: [
      {
        search: 'This field is required.',
        replace: 'Dieses Feld wird benötigt.',
      },
      {
        search: 'Must be valid email.',
        replace:
          "Muss eine gültige E-Mail sein. <span class = 'mktoErrorDetail'> example@yourdomain.com </span>",
      },
    ],
    es: [
      {
        search: 'This field is required.',
        replace: 'Este campo es obligatorio.',
      },
      {
        search: 'Must be valid email.',
        replace:
          "Debe ser un correo electrónico válido. <span class='mktoErrorDetail'> ejemplo@tudominio.com </span>",
      },
    ],
    pt: [
      {
        search: 'This field is required.',
        replace: 'Este campo é obrigatório.',
      },
      {
        search: 'Must be valid email.',
        replace:
          "Deve ser um e-mail válido. <span class='mktoErrorDetail'> exemplo@seudominio.com </span>",
      },
    ],
  };

  const selectFieldTranslations = {
    fr: {
      Country: {
        'Select...': 'Sélectionner...',
        'United States': 'États-Unis',
        'United Kingdom': 'Royaume Uni',
        Canada: 'Canada',
        France: 'France',
        Germany: 'Allemagne',
        Afghanistan: 'Afghanistan',
        'Åland Islands': 'Îles Åland',
        Albania: 'Albanie',
        Algeria: 'Algérie',
        'American Samoa': 'Samoa américaine',
        Andorra: 'Andorre',
        Angola: 'Angola',
        Anguilla: 'Anguilla',
        'Antigua and Barbuda': 'Antigua-et-Barbuda',
        Argentina: 'Argentine',
        Armenia: 'Arménie',
        Aruba: 'Aruba',
        Australia: 'Australie',
        Austria: 'Autriche',
        Azerbaijan: 'Azerbaïdjan',
        Bahamas: 'Bahamas',
        Bahrain: 'Bahrayn',
        Bangladesh: 'Bangladesh',
        Barbados: 'Barbade',
        Belarus: 'Biélorussie',
        Belgium: 'Belgique',
        Belize: 'Belize',
        Benin: 'Bénin',
        Bermuda: 'Bermudes',
        Bhutan: 'Bhoutan',
        'Bolivia, Plurinational State of': 'État plurinational de Bolivie',
        'Bosnia and Herzegovina': 'Bosnie-Herzégovine',
        Botswana: 'Botswana',
        'Bouvet Island': 'Île Bouvet',
        Brazil: 'Brésil',
        'British Indian Ocean Territory':
          "Territoire britannique de l'océan Indien",
        'Brunei Darussalam': 'Brunei Darussalam',
        Bulgaria: 'Bulgarie',
        'Burkina Faso': 'Burkina Faso',
        Burundi: 'Burundi',
        Cambodia: 'Cambodge',
        Cameroon: 'Cameroun',
        'Cape Verde': 'Cap-Vert',
        'Cayman Islands': 'Îles Caïmans',
        'Central African Republic': 'République centrafricaine',
        Chad: 'Tchad',
        Chile: 'Chili',
        China: 'Chine',
        'Christmas Island': 'Île Christmas',
        'Cocos (Keeling) Islands': 'Îles Cocos',
        Colombia: 'Colombie',
        Comoros: 'Comores',
        'Congo, the Democratic Republic of the':
          'République Démocratique du Congo',
        Congo: 'Congo',
        'Cook Islands': 'Îles Cook',
        'Costa Rica': 'Costa-Rica',
        'Cote d’Ivoire': "Côte d'Ivoire",
        Croatia: 'Croatie',
        Curacao: 'Curaçao',
        Cyprus: 'Chypre',
        'Czech Republic': 'République Tchèque',
        Denmark: 'Danemark',
        Djibouti: 'Djibouti',
        Dominica: 'Dominique',
        'Dominican Republic': 'République dominicaine',
        Ecuador: 'Équateur',
        Egypt: 'Égypte',
        'El Salvador': 'Salvador',
        'Equatorial Guinea': 'Guinée Équatoriale',
        Eritrea: 'Érythrée',
        Estonia: 'Estonie',
        Ethiopia: 'Éthiopie',
        'Falkland Islands (Malvinas)': 'Îles Malouines',
        'Faroe Islands': 'Îles Féroé',
        Fiji: 'Fidji',
        Finland: 'Finlande',
        'French Guiana': 'Guyane française',
        'French Polynesia': 'Polynésie française',
        'French Southern Territories': 'Terres australes françaises',
        Gabon: 'Gabon',
        Gambia: 'Gambie',
        Georgia: 'Géorgie',
        Ghana: 'Ghana',
        Gibraltar: 'Gibraltar',
        Greece: 'Grèce',
        Greenland: 'Groenland',
        Grenada: 'Grenade',
        Guadeloupe: 'Guadeloupe',
        Guam: 'Guam',
        Guatemala: 'Guatemala',
        Guernsey: 'Guernesey',
        Guinea: 'Guinée',
        'Guinea-Bissau': 'Guinée-Bissau',
        Guyana: 'Guyane',
        Haiti: 'Haïti',
        'Heard Island and McDonald Islands': 'Îles Heard et McDonald',
        'Holy See (Vatican City State)': 'État de la Cité du Vatican',
        Honduras: 'Honduras',
        'Hong Kong': 'Hong Kong',
        Hungary: 'Hongrie',
        Iceland: 'Islande',
        India: 'Inde',
        Indonesia: 'Indonésie',
        Iraq: 'Irak',
        Ireland: 'Irlande',
        'Isle of Man': 'Île de Man',
        Israel: 'Israël',
        Italy: 'Italie',
        Jamaica: 'Jamaïque',
        Japan: 'Japon',
        Jersey: 'Jersey',
        Jordan: 'Jordanie',
        Kazakhstan: 'Kazakhstan',
        Kenya: 'Kenya',
        Kiribati: 'Kiribati',
        'Korea, Republic of': 'République démocratique de la Corée',
        Kuwait: 'Koweït',
        Kyrgyzstan: 'Kirghizstan',
        'Lao People’s Democratic Republic':
          'République démocratique populaire de Lao',
        Latvia: 'Lettonie',
        Lebanon: 'Liban',
        Lesotho: 'Lesotho',
        Liberia: 'Liberia',
        Libya: 'Libye',
        Liechtenstein: 'Liechtenstein',
        Lithuania: 'Lituanie',
        Luxembourg: 'Luxembourg',
        Macao: 'Macao',
        'Macedonia, the former Yugoslav Republic of':
          'ex-République yougoslave de Macédoine',
        Madagascar: 'Madagascar',
        Malawi: 'Malawi',
        Malaysia: 'Malaisie',
        Maldives: 'Maldives',
        Mali: 'Mali',
        Malta: 'Malte',
        'Marshall Islands': 'Îles Marshall',
        Martinique: 'Martinique',
        Mauritania: 'Mauritanie',
        Mauritius: 'Île Maurice',
        Mayotte: 'Mayotte',
        Mexico: 'Mexique',
        'Micronesia, Federated States of': 'États fédérés de Micronésie',
        'Moldova, Republic of': 'République de Moldavie',
        Monaco: 'Monaco',
        Mongolia: 'Mongolie',
        Montenegro: 'Monténégro',
        Montserrat: 'Montserrat',
        Morocco: 'Maroc',
        Mozambique: 'Mozambique',
        Myanmar: 'Myanmar',
        Namibia: 'Namibie',
        Nauru: 'Nauru',
        Nepal: 'Népal',
        Netherlands: 'Pays Bas',
        'New Caledonia': 'Nouvelle Calédonie',
        'New Zealand': 'Nouvelle Zélande',
        Nicaragua: 'Nicaragua',
        Niger: 'Niger',
        Nigeria: 'Nigeria',
        Niue: 'Nioué',
        'Norfolk Island': 'Île Norfolk',
        'Northern Mariana Islands': 'Îles Mariannes du Nord',
        Norway: 'Norvège',
        Oman: 'Oman',
        Pakistan: 'Pakistan',
        Palau: 'Palaos',
        'Palestinian Territory, Occupied': 'Territoires palestiniens occupés',
        Panama: 'Panama',
        'Papua New Guinea': 'Papouasie-Nouvelle-Guinée',
        Paraguay: 'Paraguay',
        Peru: 'Pérou',
        Philippines: 'Philippines',
        Pitcairn: 'Pitcairn',
        Poland: 'Pologne',
        Portugal: 'Portugal',
        'Puerto Rico': 'Porto Rico',
        Qatar: 'Qatar',
        Réunion: 'Réunion',
        Romania: 'Roumanie',
        'Russian Federation': 'Fédération de Russie',
        Rwanda: 'Rwanda',
        'Saint Barthélemy': 'Saint-Barthélemy',
        'Saint Helena, Ascension and Tristan da Cunha':
          'Sainte-Hélène, Ascension et Tristan da Cunha',
        'Saint Kitts and Nevis': 'Saint-Kitts-et-Nevis',
        'Saint Lucia': 'Sainte-Lucie',
        'Saint Martin (French part)': 'Saint Martin (Partie française)',
        'Saint Pierre and Miquelon': 'Saint-Pierre-et-Miquelon',
        'Saint Vincent and the Grenadines': 'Saint-Vincent-et-les Grenadines',
        Samoa: 'Samoa',
        'San Marino': 'Saint Marin',
        'Sao Tome and Principe': 'Sao Tomé-et-Principe',
        'Saudi Arabia': 'Arabie Saoudite',
        Senegal: 'Sénégal',
        Serbia: 'Serbie',
        Seychelles: 'Seychelles',
        'Sierra Leone': 'Sierra Léone',
        Singapore: 'Singapour',
        'Sint Maarten (Dutch part)': 'Saint-Martin (Royaume des Pays-Bas)',
        Slovakia: 'Slovaquie',
        Slovenia: 'Slovénie',
        'Solomon Islands': 'Îles Salomon',
        Somalia: 'Somalie',
        'South Africa': 'Afrique du Sud',
        'South Georgia and the South Sandwich Islands':
          'Géorgie du Sud-et-les Îles Sandwich du Sud',
        'South Sudan': 'Soudan du Sud',
        Spain: 'Espagne',
        'Sri Lanka': 'Sri Lanka',
        Suriname: 'Suriname',
        'Svalbard and Jan Mayen': 'Svalbard et Jan Mayen',
        Swaziland: 'Swaziland',
        Sweden: 'Suède',
        Switzerland: 'Suisse',
        Taiwan: 'Taïwan',
        Tajikistan: 'Tadjikistan',
        'Tanzania, United Republic of': 'République unie de Tanzanie',
        Thailand: 'Thaïlande',
        'Timor-Leste': 'Timor-Leste',
        Togo: 'Togo',
        Tokelau: 'Tokelau',
        Tonga: 'Tonga',
        'Trinidad and Tobago': 'Trinité-et-Tobago',
        Tunisia: 'Tunisie',
        Turkey: 'Turquie',
        Turkmenistan: 'Turkménistan',
        'Turks and Caicos Islands': 'Îles Turques-et-Caïques',
        Tuvalu: 'Tuvalu',
        Uganda: 'Ouganda',
        Ukraine: 'Ukraine',
        'United Arab Emirates': 'Émirats arabes unis',
        Uruguay: 'Uruguay',
        Uzbekistan: 'Ouzbékistan',
        Vanuatu: 'Vanuatu',
        'Venezuela, Bolivarian Republic of':
          'République bolivarienne du Venezuela',
        'Viet Nam': 'Viet Nam',
        'Virgin Islands, British': 'Îles vierges (britanniques)',
        'Wallis and Futuna': 'Wallis-et-Futuna',
        'Western Sahara': 'Sahara occidental',
        Yemen: 'Yémen',
        Zambia: 'Zambie',
        Zimbabwe: 'Zimbabwe',
      },
    },
    de: {
      Country: {
        'Select...': 'Wählen...',
        'United States': 'Vereinigte Staaten',
        'United Kingdom': 'Königreich Großbritannien',
        Canada: 'Kanada',
        France: 'Frankreich',
        Germany: 'Deutschland',
        Afghanistan: 'Afghanistan',
        'Åland Islands': 'Åland-Inseln',
        Albania: 'Albanien',
        Algeria: 'Algerien',
        'American Samoa': 'Amerikanisch-Samoa',
        Andorra: 'Andorra',
        Angola: 'Angola',
        Anguilla: 'Anguilla',
        'Antigua and Barbuda': 'Antigua und Barbuda',
        Argentina: 'Argentinien',
        Armenia: 'Armenien',
        Aruba: 'Aruba',
        Australia: 'Australien',
        Austria: 'Österreich',
        Azerbaijan: 'Aserbaidschan',
        Bahamas: 'Bahamas',
        Bahrain: 'Bahrain',
        Bangladesh: 'Bangladesch',
        Barbados: 'Barbados',
        Belarus: 'Weißrussland',
        Belgium: 'Belgien',
        Belize: 'Belize',
        Benin: 'Benin',
        Bermuda: 'Bermuda',
        Bhutan: 'Bhutan',
        'Bolivia, Plurinational State of': 'Bolivien',
        'Bosnia and Herzegovina': 'Bosnien und Herzegowina',
        Botswana: 'Botswana',
        'Bouvet Island': 'Bouvetinsel',
        Brazil: 'Brasilien',
        'British Indian Ocean Territory':
          'Britisches Territorium im Indischen Ozean',
        'Brunei Darussalam': 'Brunei',
        Bulgaria: 'Bulgarien',
        'Burkina Faso': 'Burkina Faso',
        Burundi: 'Burundi',
        Cambodia: 'Kambodscha',
        Cameroon: 'Kamerun',
        'Cape Verde': 'Kap Verde',
        'Cayman Islands': 'Kaimaninseln',
        'Central African Republic': 'Zentralafrikanische Republik',
        Chad: 'Tschad',
        Chile: 'Chile',
        China: 'China',
        'Christmas Island': 'Weihnachtsinsel',
        'Cocos (Keeling) Islands': 'Kokosinseln',
        Colombia: 'Kolumbien',
        Comoros: 'Komoren',
        'Congo, the Democratic Republic of the': 'Demokratische Republik Kongo',
        Congo: 'Republik Kongo',
        'Cook Islands': 'Cookinseln',
        'Costa Rica': 'Costa Rica',
        "Cote d'Ivoire": 'Elfenbeinküste',
        Croatia: 'Kroatien',
        Curacao: 'Curaçao',
        Cyprus: 'Zypern',
        'Czech Republic': 'Tschechien',
        Denmark: 'Dänemark',
        Djibouti: 'Dschibuti',
        Dominica: 'Dominica',
        'Dominican Republic': 'Dominikanische Republik',
        Ecuador: 'Ecuador',
        Egypt: 'Ägypten',
        'El Salvador': 'El Salvador',
        'Equatorial Guinea': 'Äquatorialguinea',
        Eritrea: 'Eritrea',
        Estonia: 'Estland',
        Ethiopia: 'Äthiopien',
        'Falkland Islands (Malvinas)': 'Falklandinseln',
        'Faroe Islands': 'Färöer',
        Fiji: 'Fidschi',
        Finland: 'Finnland',
        'French Guiana': 'Französisch-Guayana',
        'French Polynesia': 'Französisch-Polynesien',
        'French Southern Territories': 'Französische Süd- und Antarktisgebiete',
        Gabon: 'Gabun',
        Gambia: 'Gambia',
        Georgia: 'Georgien',
        Ghana: 'Ghana',
        Gibraltar: 'Gibraltar',
        Greece: 'Griechenland',
        Greenland: 'Grönland',
        Grenada: 'Grenada',
        Guadeloupe: 'Guadeloupe',
        Guam: 'Guam',
        Guatemala: 'Guatemala',
        Guernsey: 'Guernsey',
        Guinea: 'Guinea',
        'Guinea-Bissau': 'Guinea-Bissau',
        Guyana: 'Guyana',
        Haiti: 'Haiti',
        'Heard Island and McDonald Islands': 'Heard und McDonaldinseln',
        'Holy See (Vatican City State)': 'Heiliger Stuhl (Vatikanstadt)',
        Honduras: 'Honduras',
        'Hong Kong': 'Hongkong',
        Hungary: 'Ungarn',
        Iceland: 'Island',
        India: 'Indien',
        Indonesia: 'Indonesien',
        Iraq: 'Irak',
        Ireland: 'Irland',
        'Isle of Man': 'Isle of Man',
        Israel: 'Israel',
        Italy: 'Italien',
        Jamaica: 'Jamaika',
        Japan: 'Japan',
        Jersey: 'Jersey',
        Jordan: 'Jordanien',
        Kazakhstan: 'Kasachstan',
        Kenya: 'Kenia',
        Kiribati: 'Kiribati',
        'Korea, Republic of': 'Südkorea',
        Kuwait: 'Kuwait',
        Kyrgyzstan: 'Kirgisistan',
        'Lao People’s Democratic Republic': 'Laos',
        Latvia: 'Lettland',
        Lebanon: 'Libanon',
        Lesotho: 'Lesotho',
        Liberia: 'Liberia',
        Libya: 'Libyen',
        Liechtenstein: 'Liechtenstein',
        Lithuania: 'Litauen',
        Luxembourg: 'Luxemburg',
        Macao: 'Macao',
        'Macedonia, the former Yugoslav Republic of': 'Mazedonien',
        Madagascar: 'Madagaskar',
        Malawi: 'Malawi',
        Malaysia: 'Malaysia',
        Maldives: 'Malediven',
        Mali: 'Mali',
        Malta: 'Malta',
        'Marshall Islands': 'Marshallinseln',
        Martinique: 'Martinique',
        Mauritania: 'Mauretanien',
        Mauritius: 'Mauritius',
        Mayotte: 'Mayotte',
        Mexico: 'Mexiko',
        'Micronesia, Federated States of': 'Mikronesien',
        'Moldova, Republic of': 'Moldawien',
        Monaco: 'Monaco',
        Mongolia: 'Mongolei',
        Montenegro: 'Montenegro',
        Montserrat: 'Montserrat',
        Morocco: 'Marokko',
        Mozambique: 'Mosambik',
        Myanmar: 'Myanmar',
        Namibia: 'Namibia',
        Nauru: 'Nauru',
        Nepal: 'Nepal',
        Netherlands: 'Niederlande',
        'New Caledonia': 'Neukaledonien',
        'New Zealand': 'Neuseeland',
        Nicaragua: 'Nicaragua',
        Niger: 'Niger',
        Nigeria: 'Nigeria',
        Niue: 'Niue',
        'Norfolk Island': 'Norfolkinsel',
        'Northern Mariana Islands': 'Nördliche Marianen',
        Norway: 'Norwegen',
        Oman: 'Oman',
        Pakistan: 'Pakistan',
        Palau: 'Palau',
        'Palestinian Territory, Occupied': 'Palästinensische Gebiete',
        Panama: 'Panama',
        'Papua New Guinea': 'Papua-Neuguinea',
        Paraguay: 'Paraguay',
        Peru: 'Peru',
        Philippines: 'Philippinen',
        Pitcairn: 'Pitcairn',
        Poland: 'Polen',
        Portugal: 'Portugal',
        'Puerto Rico': 'Puerto Rico',
        Qatar: 'Katar',
        Réunion: 'Réunion',
        Romania: 'Rumänien',
        'Russian Federation': 'Russland',
        Rwanda: 'Ruanda',
        'Saint Barthélemy': 'Saint-Barthélemy',
        'Saint Helena, Ascension and Tristan da Cunha':
          'St. Helena, Ascension und Tristan da Cunha',
        'Saint Kitts and Nevis': 'St. Kitts und Nevis',
        'Saint Lucia': 'St. Lucia',
        'Saint Martin (French part)': 'Saint Martin (französischer Teil)',
        'Saint Pierre and Miquelon': 'St. Pierre und Miquelon',
        'Saint Vincent and the Grenadines': 'St. Vincent und die Grenadinen',
        Samoa: 'Samoa',
        'San Marino': 'San Marino',
        'Sao Tome and Principe': 'São Tomé und Príncipe',
        'Saudi Arabia': 'Saudi-Arabien',
        Senegal: 'Senegal',
        Serbia: 'Serbien',
        Seychelles: 'Seychellen',
        'Sierra Leone': 'Sierra Leone',
        Singapore: 'Singapur',
        'Sint Maarten (Dutch part)': 'Sint Maarten',
        Slovakia: 'Slowakei',
        Slovenia: 'Slowenien',
        'Solomon Islands': 'Salomonen',
        Somalia: 'Somalia',
        'South Africa': 'Südafrika',
        'South Georgia and the South Sandwich Islands':
          'Südgeorgien und die Südlichen Sandwichinseln',
        'South Sudan': 'Südsudan',
        Spain: 'Spanien',
        'Sri Lanka': 'Sri Lanka',
        Suriname: 'Suriname',
        'Svalbard and Jan Mayen': 'Svalbard und Jan Mayen',
        Swaziland: 'Swasiland',
        Sweden: 'Schweden',
        Switzerland: 'Schweiz',
        Taiwan: 'Taiwan',
        Tajikistan: 'Tadschikistan',
        'Tanzania, United Republic of': 'Tansania',
        Thailand: 'Thailand',
        'Timor-Leste': 'Osttimor',
        Togo: 'Togo',
        Tokelau: 'Tokelau',
        Tonga: 'Tonga',
        'Trinidad and Tobago': 'Trinidad und Tobago',
        Tunisia: 'Tunesien',
        Turkey: 'Türkei',
        Turkmenistan: 'Turkmenistan',
        'Turks and Caicos Islands': 'Turks- und Caicosinseln',
        Tuvalu: 'Tuvalu',
        Uganda: 'Uganda',
        Ukraine: 'Ukraine',
        'United Arab Emirates': 'Vereinigte Arabische Emirate',
        Uruguay: 'Uruguay',
        Uzbekistan: 'Usbekistan',
        Vanuatu: 'Vanuatu',
        'Venezuela, Bolivarian Republic of': 'Venezuela',
        'Viet Nam': 'Vietnam',
        'Virgin Islands, British': 'Britische Jungferninseln',
        'Wallis and Futuna': 'Wallis und Futuna',
        'Western Sahara': 'Westsahara',
        Yemen: 'Jemen',
        Zambia: 'Sambia',
        Zimbabwe: 'Simbabwe',
      },
    },
    it: {
      Country: {
        'Select...': 'Seleziona...',
        'United States': 'Stati Uniti',
        'United Kingdom': 'Regno Unito',
        Canada: 'Canada',
        France: 'Francia',
        Germany: 'Germania',
        Afghanistan: 'Afghanistan',
        'Åland Islands': 'Isole Åland',
        Albania: 'Albania',
        Algeria: 'Algeria',
        'American Samoa': 'Samoa Americane',
        Andorra: 'Andorra',
        Angola: 'Angola',
        Anguilla: 'Anguilla',
        'Antigua and Barbuda': 'Antigua e Barbuda',
        Argentina: 'Argentina',
        Armenia: 'Armenia',
        Aruba: 'Aruba',
        Australia: 'Australia',
        Austria: 'Austria',
        Azerbaijan: 'Azerbaijan',
        Bahamas: 'Bahamas',
        Bahrain: 'Bahrein',
        Bangladesh: 'Bangladesh',
        Barbados: 'Barbados',
        Belarus: 'Bielorussia',
        Belgium: 'Belgio',
        Belize: 'Belize',
        Benin: 'Benin',
        Bermuda: 'Bermuda',
        Bhutan: 'Bhutan',
        'Bolivia, Plurinational State of': 'Bolivia',
        'Bosnia and Herzegovina': 'Bosnia ed Erzegovina',
        Botswana: 'Botswana',
        'Bouvet Island': 'Isola Bouvet',
        Brazil: 'Brasile',
        'British Indian Ocean Territory':
          "Territori Britannici dell'Oceano Indiano",
        'Brunei Darussalam': 'Brunei',
        Bulgaria: 'Bulgaria',
        'Burkina Faso': 'Burkina Faso',
        Burundi: 'Burundi',
        Cambodia: 'Cambogia',
        Cameroon: 'Camerun',
        'Cape Verde': 'Capo Verde',
        'Cayman Islands': 'Isole Cayman',
        'Central African Republic': 'Repubblica Centrafricana',
        Chad: 'Ciad',
        Chile: 'Cile',
        China: 'Cina',
        'Christmas Island': 'Isola di Natale',
        'Cocos (Keeling) Islands': 'Isole Cocos',
        Colombia: 'Colombia',
        Comoros: 'Comore',
        'Congo, the Democratic Republic of the':
          'Repubblica Democratica del Congo',
        Congo: 'Repubblica del Congo',
        'Cook Islands': 'Isole Cook',
        'Costa Rica': 'Costa Rica',
        "Cote d'Ivoire": "Costa d'Avorio",
        Croatia: 'Croazia',
        Curacao: 'Curacao',
        Cyprus: 'Cipro',
        'Czech Republic': 'Cechia, Repubblica Ceca',
        Denmark: 'Danimarca',
        Djibouti: 'Gibuti',
        Dominica: 'Dominica',
        'Dominican Republic': 'Repubblica Dominicana',
        Ecuador: 'Ecuador',
        Egypt: 'Egitto',
        'El Salvador': 'El Salvador',
        'Equatorial Guinea': 'Guinea Equatoriale',
        Eritrea: 'Eritrea',
        Estonia: 'Estonia',
        Ethiopia: 'Etiopia',
        'Falkland Islands (Malvinas)': 'Isole Falkland',
        'Faroe Islands': 'Isole Faroe',
        Fiji: 'Isole Fiji',
        Finland: 'Finlandia',
        'French Guiana': 'Guyana Francese',
        'French Polynesia': 'Polinesia Francese',
        'French Southern Territories': 'Terre Australi e Antartiche Francesi',
        Gabon: 'Gabon',
        Gambia: 'Gambia',
        Georgia: 'Georgia',
        Ghana: 'Ghana',
        Gibraltar: 'Gibilterra',
        Greece: 'Grecia',
        Greenland: 'Groenlandia',
        Grenada: 'Grenada',
        Guadeloupe: 'Guadalupa',
        Guam: 'Guam',
        Guatemala: 'Guatemala',
        Guernsey: 'Guernsey',
        Guinea: 'Guinea',
        'Guinea-Bissau': 'Guinea Bissau',
        Guyana: 'Guyana',
        Haiti: 'Haiti',
        'Heard Island and McDonald Islands': 'Isole Heard e McDonald',
        'Holy See (Vatican City State)': "Santa Sede, Vaticano (Citta' del)",
        Honduras: 'Honduras',
        'Hong Kong': 'Hong Kong',
        Hungary: 'Ungheria',
        Iceland: 'Islanda',
        India: 'India',
        Indonesia: 'Indonesia',
        Iraq: 'Iraq',
        Ireland: 'Irlanda',
        'Isle of Man': 'Isola di Man',
        Israel: 'Israele',
        Italy: 'Italia',
        Jamaica: 'Giamaica',
        Japan: 'Giappone',
        Jersey: 'Jersey',
        Jordan: 'Giordania',
        Kazakhstan: 'Kazakhstan',
        Kenya: 'Kenya',
        Kiribati: 'Kiribati',
        'Korea, Republic of': 'Repubblica di Corea (Corea del Sud)',
        Kuwait: 'Kuwait',
        Kyrgyzstan: 'Kirghizistan',
        'Lao People’s Democratic Republic':
          'Laos (Repubblica Popolare Democratica del Laos)',
        Latvia: 'Lettonia',
        Lebanon: 'Libano',
        Lesotho: 'Lesotho',
        Liberia: 'Liberia',
        Libya: 'Libia',
        Liechtenstein: 'Liechtenstein',
        Lithuania: 'Lituania',
        Luxembourg: 'Lussemburgo',
        Macao: 'Macao',
        'Macedonia, the former Yugoslav Republic of': 'Repubblica di Macedonia',
        Madagascar: 'Madagascar',
        Malawi: 'Malawi',
        Malaysia: 'Malesia',
        Maldives: 'Maldive',
        Mali: 'Mali',
        Malta: 'Malta',
        'Marshall Islands': 'Isole Marshall',
        Martinique: 'Martinica',
        Mauritania: 'Mauritania',
        Mauritius: 'Mauritius',
        Mayotte: 'Mayotte',
        Mexico: 'Messico',
        'Micronesia, Federated States of': 'Micronesia (Isole)',
        'Moldova, Republic of': 'Moldova',
        Monaco: 'Monaco',
        Mongolia: 'Mongolia',
        Montenegro: 'Montenegro',
        Montserrat: 'Montserrat',
        Morocco: 'Marocco',
        Mozambique: 'Mozambico',
        Myanmar: 'Birmania (Myanmar)',
        Namibia: 'Namibia',
        Nauru: 'Nauru',
        Nepal: 'Nepal',
        Netherlands: 'Paesi Bassi (Olanda)',
        'New Caledonia': 'Nuova Caledonia',
        'New Zealand': 'Nuova Zelanda',
        Nicaragua: 'Nicaragua',
        Niger: 'Niger',
        Nigeria: 'Nigeria',
        Niue: 'Niue',
        'Norfolk Island': 'Isola Norfolk',
        'Northern Mariana Islands': 'Isole Marianne Settentrionali',
        Norway: 'Norvegia',
        Oman: 'Oman',
        Pakistan: 'Pakistan',
        Palau: 'Palau',
        'Palestinian Territory, Occupied': 'Territori palestinesi',
        Panama: 'Panama',
        'Papua New Guinea': 'Papua Nuova Guinea',
        Paraguay: 'Paraguay',
        Peru: 'Peru',
        Philippines: 'Filippine',
        Pitcairn: 'Isole Pitcairn',
        Poland: 'Polonia',
        Portugal: 'Portogallo',
        'Puerto Rico': 'Porto Rico',
        Qatar: 'Qatar',
        Réunion: 'Riunione (isola)',
        Romania: 'Romania',
        'Russian Federation': 'Russia',
        Rwanda: 'Ruanda',
        'Saint Barthélemy': 'Saint Barthélemy',
        'Saint Helena, Ascension and Tristan da Cunha':
          "Sant'Elena, Ascensione e Tristan da Cunha",
        'Saint Kitts and Nevis': 'Saint Kitts e Nevis',
        'Saint Lucia': 'Santa Lucia',
        'Saint Martin (French part)': 'Saint Martin (parte francese)',
        'Saint Pierre and Miquelon': 'Saint Pierre e Miquelon',
        'Saint Vincent and the Grenadines': 'Saint Vincent e Grenadine',
        Samoa: 'Samoa',
        'San Marino': 'San Marino',
        'Sao Tome and Principe': "Sao Tome' e Principe",
        'Saudi Arabia': 'Arabia Saudita',
        Senegal: 'Senegal',
        Serbia: 'Serbia',
        Seychelles: 'Seychelles',
        'Sierra Leone': 'Sierra Leone',
        Singapore: 'Singapore',
        'Sint Maarten (Dutch part)': 'Sint Maarten (parte olandese)',
        Slovakia: 'Slovacchia',
        Slovenia: 'Slovenia',
        'Solomon Islands': 'Isole Salomone',
        Somalia: 'Somalia',
        'South Africa': 'Sudafrica',
        'South Georgia and the South Sandwich Islands':
          'Georgia del Sud e Isole Sandwich Australi',
        'South Sudan': 'Sudan del Sud',
        Spain: 'Spagna',
        'Sri Lanka': 'Sri Lanka',
        Suriname: 'Suriname',
        'Svalbard and Jan Mayen': 'Svalbard e Jan Mayen',
        Swaziland: 'Swaziland',
        Sweden: 'Svezia',
        Switzerland: 'Svizzera',
        Taiwan: 'Taiwan, Repubblica di Cina',
        Tajikistan: 'Tagikistan',
        'Tanzania, United Republic of': 'Tanzania',
        Thailand: 'Thailandia',
        'Timor-Leste': 'Timor Est',
        Togo: 'Togo',
        Tokelau: 'Tokelau',
        Tonga: 'Tonga',
        'Trinidad and Tobago': 'Trinidad e Tobago',
        Tunisia: 'Tunisia',
        Turkey: 'Turchia',
        Turkmenistan: 'Turkmenistan',
        'Turks and Caicos Islands': 'Isole Turks and Caicos',
        Tuvalu: 'Tuvalu',
        Uganda: 'Uganda',
        Ukraine: 'Ucraina',
        'United Arab Emirates': 'Emirati Arabi Uniti',
        Uruguay: 'Uruguay',
        Uzbekistan: 'Uzbekistan',
        Vanuatu: 'Vanuatu',
        'Venezuela, Bolivarian Republic of': 'Venezuela',
        'Viet Nam': 'Vietnam',
        'Virgin Islands, British': 'Isole Vergini Britanniche',
        'Wallis and Futuna': 'Wallis e Futuna',
        'Western Sahara': 'Sahara Occidentale',
        Yemen: 'Yemen',
        Zambia: 'Zambia',
        Zimbabwe: 'Zimbabwe',
      },
    },
  };

  // Set 'checkbox-type' attribute for checkbox labels
  MktoForms2.whenRendered(function (mktoForm) {
    const formEl = mktoForm.getFormElem()[0];

    const labels = formEl.querySelectorAll("label[for*='Checkbox']");
    labels.forEach((label) => {
      const relatedInput = formEl.querySelector('#' + label.htmlFor) ||
        formEl.querySelector("[name='" + label.htmlFor + "']") || { type: '' };
      if (relatedInput.type === 'checkbox') {
        label.setAttribute('checkbox-type', relatedInput.name);
      }
    });
  });

  // Update labels function
  function updateLabels(formEl, translations) {
    translations.forEach(({ mktoFieldId, label, description }) => {
      let labelElement;

      if (description === 'checkbox') {
        labelElement = formEl.querySelector(
          `label[checkbox-type='${mktoFieldId}']`,
        );
      } else {
        labelElement = formEl.querySelector(`label[for='${mktoFieldId}']`);
      }

      if (labelElement) {
        const childNodes = Array.from(labelElement.childNodes);

        // Handle cases where the label has no child nodes
        if (childNodes.length > 0) {
          const editableNodeRange = document.createRange();
          const firstEditableNode =
            description === 'checkbox'
              ? childNodes[0]
              : childNodes[1] || childNodes[0];
          const lastEditableNode = labelElement.lastChild;

          if (firstEditableNode && lastEditableNode) {
            editableNodeRange.setStartBefore(firstEditableNode);
            editableNodeRange.setEndAfter(lastEditableNode);
            editableNodeRange.deleteContents();
          }

          // Create and insert the new label fragment
          const newLabelFragment =
            editableNodeRange.createContextualFragment(label);
          editableNodeRange.insertNode(newLabelFragment);
        } else {
          // If no child nodes exist, just set the label text
          labelElement.textContent = label;
        }
      }
    });
  }

  // Update validation messages function
  function updateValidationMessages(fieldDescriptors, messages) {
    if (!messages || messages.length === 0) {
      return;
    }

    const descriptorsArray = Array.from(fieldDescriptors);

    descriptorsArray
      .map((desc) => MktoForms2.$(desc).data('mktoFieldDescriptor'))
      .filter((descJ) => descJ.validationMessage !== undefined)
      .forEach((descJ) => {
        messages.forEach(({ search, replace }) => {
          if (descJ.validationMessage) {
            descJ.validationMessage = descJ.validationMessage.replace(
              search,
              replace,
            );
          }
        });
      });
  }

  // Update select options function
  function updateSelectOptions(formEl, translations) {
    if (translations[currentLanguage]) {
      Object.keys(translations[currentLanguage]).forEach((fieldName) => {
        const selectElement = formEl.querySelector(
          `select[name='${fieldName}']`,
        );

        if (selectElement) {
          const languageSpecificTranslations =
            translations[currentLanguage][fieldName];

          selectElement.querySelectorAll('option').forEach((option) => {
            const text = option.textContent;

            if (
              languageSpecificTranslations &&
              languageSpecificTranslations[text]
            ) {
              option.textContent = languageSpecificTranslations[text];
            }
          });
        }
      });
    }
  }

  // Update button text function with defaults
  function updateButtonText(formEl, translations) {
    const buttonElement = formEl.querySelector('.mktoButton');

    if (buttonElement) {
      const buttonText = buttonElement.textContent.trim();
      const buttonFieldIds = {
        Register: 'submitButtonRegister',
        'Please wait': 'submitButtonPleaseWait',
        'Get in Touch': 'submitButtonGetInTouch',
        'Get Started': 'submitButtonGetStarted',
      };

      const buttonTranslation = translations.find(
        ({ mktoFieldId }) => mktoFieldId === buttonFieldIds[buttonText],
      );

      if (buttonTranslation && buttonTranslation.label) {
        buttonElement.textContent = buttonTranslation.label;
      }
    }
  }

  // Update HTML content function based on form ID and multiple classes
  function updateHtmlElement(formEl, translations) {
    translations.forEach((translation) => {
      const elements = formEl.querySelectorAll(`.${translation.mktoFieldId}`); // Use class name from mktoFieldId

      if (elements.length > 0) {
        elements.forEach((element) => {
          element.innerHTML = translation.label; // Directly replace the content with the translation
        });
      }
    });
  }

  // Update instruction text function using ID directly
  function updateInstructionText(formEl, translations) {
    const instructionElement = formEl.querySelector('#InstructEmail');

    if (instructionElement) {
      const instructionTranslation = translations.find(
        ({ mktoFieldId }) => mktoFieldId === 'InstructEmail',
      );

      if (instructionTranslation && instructionTranslation.label) {
        instructionElement.innerHTML = instructionTranslation.label; // Directly replace the content with the translation
      }
    }
  }

  MktoForms2.whenRendered((form) => {
    // Check if we're on the target URL
    if (window.location.href.includes('/solutions/gitlab-duo-pro/sales/')) {
      const formEl = form.getFormElem()[0];
      const fieldDescriptors = formEl.querySelectorAll('.mktoFieldDescriptor');

      if (translations[currentLanguage]) {
        updateLabels(formEl, translations[currentLanguage]);
        updateValidationMessages(fieldDescriptors, messages[currentLanguage]);
        updateSelectOptions(formEl, selectFieldTranslations);
        updateButtonText(formEl, translations[currentLanguage]);
        updateHtmlElement(formEl, translations[currentLanguage]);
        updateInstructionText(formEl, translations[currentLanguage]);
        // **Add Hidden Fields Based on Language**
        // Get the language name from the mapping
        const languageNames = {
          ja: 'Japanese',
          de: 'German',
          fr: 'French',
          it: 'Italian',
          pt: 'Portuguese',
          es: 'Spanish',
          // Add 'ko': 'Korean' if needed
        };
        const languageName = languageNames[currentLanguage];

        // Add hidden fields using the Marketo Forms API
        if (languageName) {
          form.addHiddenFields({
            selfReportedPreferredLanguage: 'true',
            Preferred_Language__c: languageName,
          });
        }
      }
    }
  });
}
