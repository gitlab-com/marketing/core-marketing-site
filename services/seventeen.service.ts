export function seventeenDataHelper(items: any[], metadata) {
  let hero_image_block, form_block, speakers_block, video_block;

  items.forEach((item) => {
    switch (item.fields.componentName) {
      case 'hero_image_block':
        hero_image_block = {
          title: {
            text: item.fields.subheader,
            note: item.fields.title,
          },
          subtitle: item.fields.description,
          seventeen: true,
          background: item.fields?.backgroundImage?.fields?.file?.url,
          background_mobile: '/nuxt-images/seventeen/gl17-hero-mobile.svg',
          image: {
            url: item.fields.video?.fields?.image?.fields?.file?.url,
            alt: 'GitLab 17',
          },
          action: {
            url: item.fields?.primaryCta?.fields.externalUrl,
            text: item.fields?.primaryCta?.fields.text,
            data_ga_name: item.fields?.primaryCta?.fields.dataGaName,
            data_ga_location: item.fields?.primaryCta?.fields.dataGaLocation,
            variant: 'secondary',
            icon: {
              name: item.fields?.primaryCta?.fields.iconName,
              variant: item.fields?.primaryCta?.fields.iconVariant,
            },
          },
        };
        break;
      case 'form_block':
        form_block = {
          form: {
            formId: item.fields.blockGroup[0].fields.formId,
            form_header: item.fields.blockGroup[0].fields.header,
            datalayer: item.fields.blockGroup[0].fields.formDataLayer,
            all_required: false,
            submitted_message: {
              header: item.fields.blockGroup[0].fields?.confirmationMessage,
              body: item.fields.blockGroup[0].fields?.blurb,
            },
            header_config: {
              text_align: 'left',
              text_variant: 'heading4-bold',
              text_tag: 'h4',
            },
          },
          content_aos_animation: 'zoom-in-right',
          content_aos_duration: 800,
          form_aos_animation: 'zoom-in-left',
          form_aos_duration: 800,
          metadata: {
            id_tag: 'register',
          },
          without_bg: true,
          content: item.fields.text,
          ...item.fields.customFields,
        };
        break;
      case 'speaker_block':
        speakers_block = {
          gitlabSeventeenRelease: {
            title: item.fields.header,
            cards: item.fields.card.map((card) => {
              return {
                name: card.fields.title,
                role: card.fields.subtitle,
                image: card.fields.image.fields.file.url,
              };
            }),
          },
        };
        break;
      case 'video_block':
        video_block = {
          header: item.fields.header,
          ...item.fields.video.fields,
          thumbnail: item.fields.video.fields.thumbnail.fields.image.fields,
        };
        break;
      default:
        break;
    }
  });

  const register_form_block = { ...form_block, ...speakers_block };

  return {
    title: metadata[0].fields?.title,
    ogTitle: metadata[0].fields?.ogTitle,
    description: metadata[0].fields?.description,
    ogDescription: metadata[0].fields?.ogDescription,
    title_image: metadata[0].fields?.ogImage?.fields?.image?.fields?.file.url,
    og_image: metadata[0].fields?.ogImage?.fields?.image?.fields?.file.url,
    twitter_image: metadata[0].fields?.ogImage?.fields?.image?.fields?.file.url,
    hero_image_block,
    register_form_block,
    video_block,
  };
}
