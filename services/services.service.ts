import { CONTENT_TYPES } from '~/common/content-types';
import {
  CtfForm,
  CtfHero,
  CtfSideNavigation,
} from '~/models/content-types.dto';

interface ServicePgData {
  title?: string;
  description?: string;
  hero?: CtfHero;
  side_menu?: CtfSideNavigation;
  service_cards?: any;
  form?: CtfForm;
}

function serviceCardsBuilder(services) {
  const cards = services.card.map((card) => {
    return {
      title: card.fields?.title,
      subtitle: card.fields?.subtitle,
      description: card.fields?.description,
    };
  });
  return {
    title: services?.header,
    subheader: services?.subheader,
    description: services?.description,
    id: services?.headerAnchorId,
    cards,
  };
}

function formBuilder(form) {
  return {
    id: 'contactform',
    header: form.header,
    form: {
      formId: form.formId,
      datalayer: 'services',
    },
  };
}

function heroBuilder(hero) {
  return {
    title: hero.title,
    subtitle: hero.description,
    primary_btn: {
      url: hero.primaryCta.fields.externalUrl,
      text: hero.primaryCta.fields.text,
      data_ga_name: hero.primaryCta.fields.dataGaName,
      data_ga_location: hero.primaryCta.fields.dataGaLocation,
    },
    image: {
      bordered: true,
      image_url: hero.backgroundImage.fields.file.url,
      alt: hero.backgroundImage.fields.title,
    },
  };
}

function sideNavBuilder(sideNavigation) {
  return {
    anchors: {
      text: sideNavigation.header,
      data: sideNavigation.anchors.map((anchor: any) => ({
        text: anchor.fields.linkText,
        href: anchor.fields.anchorLink,
      })),
    },
    ...sideNavigation.customFields,
  };
}

function pageBuilder(page) {
  let result: ServicePgData = { service_cards: [] };
  for (const section of page) {
    const id = section.sys.contentType?.sys.id;
    switch (id) {
      case CONTENT_TYPES.SIDE_MENU:
        result.side_menu = {
          ...sideNavBuilder(section.fields),
        };
        break;
      case CONTENT_TYPES.HERO:
        result.hero = {
          ...heroBuilder(section.fields),
        };
        break;
      case CONTENT_TYPES.CARD_GROUP:
        result.service_cards.push(serviceCardsBuilder(section.fields));
        break;
      case CONTENT_TYPES.FORM:
        result.form = {
          ...formBuilder(section.fields),
        };
        break;
    }
  }

  return result;
}

function metadataHelper(data) {
  const seo = data[0].fields;
  return {
    title: seo.title,
    og_title: seo.title,
    description: seo.description,
    twitter_description: seo.description,
    og_description: seo.description,
  };
}

export function ServicesPgHelper(data) {
  const { pageContent, seoMetadata } = data;
  const cleanPagecontent = pageBuilder(pageContent);

  const metadata = metadataHelper(seoMetadata);
  const transformedData: ServicePgData = {
    ...metadata,
    ...cleanPagecontent,
  };
  return transformedData;
}
