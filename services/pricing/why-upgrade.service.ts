import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '~/common/content-types';
import { COMPONENT_NAMES } from '~/common/constants';
import { getClient } from '~/plugins/contentful.js';
import { mapQuoteCarousel } from './../solutions/solutions-default.helper';
import {
  CtfCard,
  CtfEntry,
  CtfHeaderAndText,
  CtfHero,
  CtfCardGroup,
  CtfCustomerLogo,
  CtfQuote,
  CtfSideNavigation,
  CtfAnchorLink,
  CtfHeaderSubHeaderAndText,
  CtfButton,
} from '~/models';
import { convertToCaseSensitiveLocale } from '~/common/util';

export class WhyUpgradeService {
  private readonly $ctx: Context;

  private readonly CONTENT_IDS = {
    hero: CONTENT_TYPES.HERO,
    cardGroup: CONTENT_TYPES.CARD_GROUP,
    headerAndText: CONTENT_TYPES.HEADER_AND_TEXT,
    card: CONTENT_TYPES.CARD,
    nextSteps: CONTENT_TYPES.NEXT_STEPS,
    customerLogos: CONTENT_TYPES.CUSTOMER_LOGOS,
    quote: CONTENT_TYPES.QUOTE,
    headerSubHeaderText: CONTENT_TYPES.HEADER_SUBHEADER_AND_TEXT,
    sideNavigation: CONTENT_TYPES.SIDE_MENU,
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  /**
   * Main method that returns components to the page
   * @param slug
   */
  async getContent(slug: string) {
    try {
      return this.getContentfulData(slug);
    } catch (e) {
      throw new Error(e);
    }
  }

  private async getContentfulData(_slug: string) {
    const locale = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    try {
      const props = {
        content_type: CONTENT_TYPES.PAGE,
        'fields.slug': _slug,
        include: 4,
        locale,
      };

      const content = await getClient().getEntries(props);

      if (content.items.length === 0) {
        throw new Error('Not found');
      }
      const [items] = content.items;
      return this.transformContentfulData(items);
    } catch (e) {
      throw new Error(e);
    }
  }

  private transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const {
      ctfHero,
      ctfCardGroupStats,
      ctfCardGroup,
      ctfHeaderSubHeaderAndText,
      ctfCard,
      ctfNextSteps,
      ctfCustomerLogos,
      ctfQuote,
      ctfRoiCalculator,
      ctfSideNavigation,
      ctfQuotesCarousel,
    } = this.getCtfComponents(pageContent);

    const transformedPage = {
      ...seoMetadata[0]?.fields,
      title: seoMetadata[0]?.fields.ogTitle,
      hero: this.mapHero(ctfHero),
      ctfCardGroup: this.mapCardGroup(ctfCardGroup),
      ctfCardGroupStats: this.mapCardGroup(ctfCardGroupStats),
      ctfCustomerLogos: this.mapCustomerLogos(ctfCustomerLogos),
      ctfRoiCalculator: this.mapRoiCalculator(ctfRoiCalculator),
      ctfQuote: this.mapSingleQuote(ctfQuote),
      headerAndText: this.mapHeaderAndText(ctfHeaderSubHeaderAndText),
      ctfSideNavigation: this.mapSideNavigation(ctfSideNavigation.fields),
      ctfQuotesCarousel: mapQuoteCarousel({ ...ctfQuotesCarousel.fields }),
      nextSteps: ctfNextSteps.sys.id,
      banner: this.mapCard(ctfCard),
    };

    return transformedPage;
  }

  private getCtfComponents(ctfComponents: any) {
    const result: {
      ctfHero?: CtfEntry<CtfHero>;
      ctfCardGroup?: CtfEntry<CtfCardGroup>;
      ctfCardGroupStats?: CtfEntry<CtfCardGroup>;
      ctfCard?: CtfCard;
      ctfNextSteps?: any;
      ctfCustomerLogos?: CtfEntry<CtfCustomerLogo>;
      ctfQuote?: CtfEntry<CtfQuote>;
      ctfHeaderSubHeaderAndText?: CtfEntry<CtfHeaderSubHeaderAndText>;
      ctfRoiCalculator: CtfEntry<CtfHeaderAndText>;
      ctfSideNavigation: CtfEntry<CtfSideNavigation>;
      ctfQuotesCarousel?: CtfEntry<CtfCardGroup>;
    } = {
      ctfCards: [],
    };

    for (const pageComponent of ctfComponents) {
      switch (pageComponent.sys.contentType.sys.id) {
        case this.CONTENT_IDS.hero:
          result.ctfHero = pageComponent;
          break;
        case this.CONTENT_IDS.cardGroup:
          result[pageComponent.fields.componentName] = pageComponent;
          break;
        case this.CONTENT_IDS.customerLogos:
          result.ctfCustomerLogos = pageComponent;
          break;
        case this.CONTENT_IDS.quote:
          result.ctfQuote = pageComponent;
          break;
        case this.CONTENT_IDS.headerAndText:
          result.ctfRoiCalculator = pageComponent;
          break;
        case this.CONTENT_IDS.headerSubHeaderText:
          result.ctfHeaderSubHeaderAndText = pageComponent;
          break;
        case this.CONTENT_IDS.sideNavigation:
          result.ctfSideNavigation = pageComponent;
          break;
        case this.CONTENT_IDS.card:
          result.ctfCard = pageComponent;
          break;
        case this.CONTENT_IDS.nextSteps:
          result.ctfNextSteps = pageComponent;
          break;
      }
    }

    return result;
  }

  private mapHero(ctfHero: CtfEntry<CtfHero>) {
    const { subheader, title, description } = ctfHero.fields;

    return {
      subheader,
      title,
      description,
      button: ctfHero.fields.primaryCta.fields,
      secondaryCta:
        ctfHero.fields.secondaryCta && ctfHero.fields.secondaryCta.fields,
      fields: {
        ...ctfHero.fields,
      },
    };
  }

  private mapCardGroup(ctfCardGroup: CtfEntry<CtfCardGroup>) {
    const { header, description, card } = ctfCardGroup.fields;
    const cards = card
      ? ctfCardGroup.fields?.card.map((card: CtfEntry<CtfCard>) => {
          const {
            title,
            subtitle,
            description,
            cardLink,
            iconName,
            cardLinkDataGaName,
            cardLinkDataGaLocation,
          } = card.fields;
          const button = card.fields.button ? card.fields.button.fields : {};
          const image = card?.fields?.image ? card.fields?.image?.fields : {};
          return {
            image,
            title,
            subtitle,
            description,
            iconName,
            cardLink,
            cardLinkDataGaName,
            cardLinkDataGaLocation,
            button,
          };
        })
      : {};

    return { header, description, cards };
  }
  private mapCustomerLogos(ctfCustomerLogos: CtfEntry<CtfCustomerLogo>) {
    const { internalName, logo } = ctfCustomerLogos.fields;

    const logos = logo.length
      ? logo.map((item) => {
          const { altText, link } = item.fields;
          const image = item.fields?.image ? item.fields.image.fields : {};
          return { altText, link, image };
        })
      : {};

    return {
      internalName,
      logos,
    };
  }
  private mapSingleQuote(ctfQuote: CtfEntry<CtfQuote>) {
    return {
      ...ctfQuote.fields,
    };
  }
  private mapRoiCalculator(
    ctfHeaderSubHeaderAndText: CtfEntry<CtfHeaderAndText>,
  ) {
    return {
      data: {
        id: 'wp-roi-calculator',
        header: {
          gradient_line: true,
          title: {
            text: ctfHeaderSubHeaderAndText.fields.header,
            anchor: 'wp-roi-calculator',
          },
          subtitle: ctfHeaderSubHeaderAndText.fields.text,
        },
        calc_data_src:
          ctfHeaderSubHeaderAndText.fields?.customFields?.data?.calc_data_src ||
          'en-us/calculator/index',
      },
    };
  }
  private mapHeaderAndText(
    ctfHeaderSubHeaderAndText: CtfEntry<CtfHeaderSubHeaderAndText>,
  ) {
    const { header, subheader, text } = ctfHeaderSubHeaderAndText.fields;

    return {
      header,
      subheader,
      text,
      cta: ctfHeaderSubHeaderAndText.fields.blocks
        ? ctfHeaderSubHeaderAndText.fields.blocks[0].fields
        : {},
    };
  }

  private mapSideNavigation(ctfSideMenu: CtfSideNavigation) {
    const anchors = ctfSideMenu.anchors.map(
      (anchor: CtfEntry<CtfAnchorLink>) => ({
        text: anchor.fields.linkText,
        href: anchor.fields.anchorLink,
        data_ga_name: anchor.fields.dataGaName,
        data_ga_location: anchor.fields.dataGaLocation,
      }),
    );
    const hyperlinks = ctfSideMenu.hyperlinks.map(
      (hyperlink: CtfEntry<CtfButton>) => ({
        text: hyperlink.fields.text,
        href: hyperlink.fields.externalUrl,
        data_ga_name: hyperlink.fields.dataGaName,
        data_ga_location: hyperlink.fields.dataGaLocation,
      }),
    );

    return {
      fields: { ...ctfSideMenu },
      name: COMPONENT_NAMES.SLP_SIDE_NAVIGATION,
      data: {
        text: ctfSideMenu.header,
        anchors: {
          data: anchors,
        },
        hyperlinks: {
          data: hyperlinks,
        },
      },
    };
  }

  private mapCard(ctfCard: any) {
    const { title } = ctfCard.fields;
    return {
      title,
      button: ctfCard.fields.button && ctfCard.fields?.button?.fields,
    };
  }
}
