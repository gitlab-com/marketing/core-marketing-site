import { COMPONENT_NAMES } from '../../common/constants';
import {
  CtfCard,
  CtfCardGroup,
  CtfEntry,
  CtfHeaderAndText,
  CtfTabControlsContainer,
} from '~/models';

export function mapSolutionsPlatformLinkGroup(ctfCardGroup: any) {
  return {
    name: COMPONENT_NAMES.SOLUTIONS_PLATFORM_LINK_GROUP,
    data: {
      header: ctfCardGroup.header,
      description: ctfCardGroup.description,
      id: ctfCardGroup.headerAnchorId,
      cards: ctfCardGroup.card,
    },
  };
}

export function mapRecognition(ctfCardGroup) {
  let badges = [];
  const cards = ctfCardGroup.card.map((card) => {
    if (card.fields.contentArea) {
      badges = card.fields.contentArea.map((badge) => {
        return {
          src: badge.fields.image.fields.file.url,
          alt: badge.fields.altText,
        };
      });
    }
    return {
      text: card.fields.description,
      logo: card.fields.image?.fields.file.url || '',
      alt: card.fields.image?.fields.title || '',
      link: {
        text: card.fields.button.fields.text,
        href: card.fields.button.fields.externalUrl,
      },
    };
  });

  const result = {
    name: COMPONENT_NAMES.RECOGNITION,
    data: {
      heading: ctfCardGroup.header,
      subtitle: ctfCardGroup.description,
      badges,
      cards: cards.filter((card) => card.text),
    },
  };
  return result;
}

export function mapSolutionsPlatformHeaderText(ctfHeaderAndText: any) {
  const { header, text } = ctfHeaderAndText;

  return {
    name: COMPONENT_NAMES.SOLUTIONS_PLATFORM_HEADER_TEXT,
    data: {
      title: header,
      subtitle: text,
    },
  };
}

export function mapSolutionsPlatformCtaCard(ctfCard: any) {
  return {
    name: COMPONENT_NAMES.SOLUTIONS_PLATFORM_CTA_CARD,
    data: {
      title: ctfCard.title,
      icon: ctfCard.iconName,
      description: ctfCard.description,
      button: {
        text: ctfCard.button.fields.text,
        href: ctfCard.button.fields.externalUrl,
        data_ga_name: ctfCard.button.fields.dataGaName,
        data_ga_location: ctfCard.button.fields.dataGaLocation,
      },
    },
  };
}

export function mapSolutionsPlatformColumnCopy(ctfCard: any) {
  return {
    name: COMPONENT_NAMES.SOLUTIONS_PLATFORM_COLUMN_COPY,
    data: {
      ...ctfCard.customFields,
      title: ctfCard.title,
      subtitle: ctfCard.cardLink,
      icon: ctfCard.iconName,
      left_header: ctfCard.subtitle,
      left_description: ctfCard.description,
      right_header: ctfCard.column2Title,
      right_description: ctfCard.column2Description,
      button: {
        text: ctfCard.button?.fields.text,
        href: ctfCard.button?.fields.externalUrl,
        data_ga_name: ctfCard.button?.fields.dataGaName,
        data_ga_location: ctfCard.button?.fields.dataGaLocation,
      },
    },
  };
}

export function mapSolutionsPlatformTabLinks(CtfTabControlsContainer: any) {
  return {
    name: COMPONENT_NAMES.SOLUTIONS_PLATFORM_TAB_LINKS,
    data: {
      links: CtfTabControlsContainer?.tabs[0]?.fields?.tabPanelContent,
    },
  };
}
