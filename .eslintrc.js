module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parser: 'vue-eslint-parser', // Parses the Vue SFC structure
  parserOptions: {
    parser: '@typescript-eslint/parser', // Parses the TypeScript within the script tags of Vue files
    sourceType: 'module',
    ecmaVersion: 2020,
  },
  extends: [
    // Base ESLint recommended rules
    'eslint:recommended',

    // Vue.js specific linting rules for ESLint
    'plugin:vue/recommended',

    // Recommended rules from the @typescript-eslint plugin
    'plugin:@typescript-eslint/recommended',

    // Must be last to override other configs
    'prettier',
  ],
  plugins: ['vue', '@typescript-eslint', 'nuxt', 'custom-rules'],
  // add your custom rules here
  rules: {
    'custom-rules/vhtml-slptypography': 'error',
    'no-console': 'error',
    'vue/multi-word-component-names': 'off',
    'prefer-const': 'error',
    'vue/no-v-html': 'off',
    'vue/no-v-text-v-html-on-component': 'off',
    'vue/prop-name-casing': 'error',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-unused-vars': [
      'error',
      { varsIgnorePattern: '^_', argsIgnorePattern: '^_' },
    ],
    'sort-imports': [
      'error',
      {
        ignoreCase: true,
        ignoreDeclarationSort: true, // This will not sort the entire import statement by the source
        ignoreMemberSort: false, // This will sort the named imports within each import statement
      },
    ],
  },
  ignorePatterns: ['*.test.js', '/plugins/**', '/app/**'],
};
